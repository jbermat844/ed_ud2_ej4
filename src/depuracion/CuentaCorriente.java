/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package depuracion;

/**
 *
 * @author luisnavarro
 */
public class CuentaCorriente {
    private double saldo;
    private static int id=0;
    private static String titular;
    public CuentaCorriente(){
        titular="desconocido";
        saldo=0;
    }
    public CuentaCorriente(String t, double saldo){
        titular=t;
        this.saldo=saldo;
    }

    public CuentaCorriente(double saldo) {
        this("desconocido", saldo);
    }
    public void ingresa(double i)
    {
        saldo+=i;
    }
    public void extrae(double e)
    {
        saldo-=e;
    }
    public void tranferir(CuentaCorriente destino, double cantidad)
    {
        saldo-=cantidad;
        destino.ingresa(cantidad);
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @return the id
     */
    public static int getId() {
        return id;
    }

    /**
     * @return the titular
     */
    public static String getTitular() {
        return titular;
    }

    /**
     * @param aTitular the titular to set
     */
    public static void setTitular(String aTitular) {
        titular = aTitular;
    }
    
}
