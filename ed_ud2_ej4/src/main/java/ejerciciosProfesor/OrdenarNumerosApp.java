/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciosProfesor;

/**
 *
 * @author Usuario
 */
import java.util.Scanner;

public class OrdenarNumerosApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingresa el primer número entero: ");
        int num1 = scanner.nextInt();
        
        System.out.print("Ingresa el segundo número entero: ");
        int num2 = scanner.nextInt();
        
        System.out.print("Ingresa el tercer número entero: ");
        int num3 = scanner.nextInt();
        
        // Llama a la función para ordenar los números
        ordenarNumeros(num1, num2, num3);
    }

    public static void ordenarNumeros(int a, int b, int c) {
        int temp;
        
        // Ordena los números en orden ascendente utilizando el método de la burbuja
        for (int i = 0; i < 2; i++) {
            if (a > b) {
                temp = a;
                a = b;
                b = temp;
            }
            if (b > c) {
                temp = b;
                b = c;
                c = temp;
            }
        }
        
        System.out.println("Los números ordenados son: " + a + ", " + b + ", " + c);
    }
}
//Este programa le pedirá al usuario que ingrese tres números enteros y luego los ordenará en orden ascendente. Asegúrate de guardar el archivo después de realizar los cambios.

//Haz clic en "Run" (Ejecutar) en la parte superior de NetBeans o presiona Shift + F6 para compilar y ejecutar el programa. Luego, sigue las instrucciones en la consola para ingresar los tres números y ver los números ordenados.
//¡Eso es todo! Ahora deberías tener un programa en NetBeans que ordena tres números enteros introducidos por el usuario.





