/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciosProfesor;

/**
 *
 * @author Usuario
 */
import java.util.Scanner;

public class ContarDigitosApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingresa un número entero (positivo o negativo, hasta 5 dígitos): ");
        int numero = scanner.nextInt();
        
        // Convierte el número a su valor absoluto para ignorar el signo
        int numeroAbsoluto = Math.abs(numero);
        
        // Convierte el número a una cadena para contar los dígitos
        String numeroComoCadena = Integer.toString(numeroAbsoluto);
        
        int cantidadDigitos = numeroComoCadena.length();
        
        System.out.println("El número tiene " + cantidadDigitos + " dígitos.");
    }
}