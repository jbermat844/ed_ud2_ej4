/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciosProfesor;

/**
 *
 * @author Usuario
 */
import java.util.Scanner;

public class Ej5masejercicios {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numero = 1;
        int numeroAnterior = 0;
        int contador= 0     ;
        
        System.out.println("Ingresa números. El programa terminará cuando ingreses 0.");

        while (numero != 0) {
            System.out.print("Ingresa un número: ");
            numero = scanner.nextInt();

            if (numero != 0) {
                int suma = numero + numeroAnterior;
                
                if (numeroAnterior != 0) {
                    System.out.println("Suma de los dos últimos números: " + suma);
                }
                
                   
                numeroAnterior = numero;
            }
           
        }

        System.out.println("Programa finalizado.");
    }
}