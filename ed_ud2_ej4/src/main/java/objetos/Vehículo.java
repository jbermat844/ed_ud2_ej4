/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package objetos;

/**
 *
 * @author Usuario
 */
public class Vehículo {

    public double getPotencia() {
        return potencia;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }

    public boolean isTieneTracción() {
        return tieneTracción;
    }

    public void setTieneTracción(boolean tieneTracción) {
        this.tieneTracción = tieneTracción;
    }
    private String modelo;
    private double potencia;
    private boolean tieneTracción;
    
    
    //Constructor
    public Vehículo(String modelo) {
        this.modelo=modelo;
    
        

    }
   
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //1.- Realiza una clase en Java de nombre Vehículo que
//contenga como atributos el modelo de tipo String, la
//potencia de tipo double y la tracción a las cuatro ruedas
//de tipo boolean. El constructor de la clase admitirá como
//argumento el modelo y tendrá como métodos de tipo dame y
//pon para la potencia y para la tracción a las cuatro
//ruedas. La clase contará con el método imprimir que retorna
//los datos de cada vehículo y si tiene o no tracción a las
//cuatro ruedas.
        Vehículo jeep=new Vehículo("jeep" );
        jeep.setPotencia(140);
        System.out.println("La potencia del jeep es: " +jeep.getPotencia());
        System.out.println("El jeep"  +jeep.tieneTracción);
        
        
    }
    
}
