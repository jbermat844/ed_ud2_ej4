
package objetos;

/**
 *
 * @author mpaniagua
 */
public class Personas {

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    //Defino los atributos
    private int edad;
    private String nombre;
    private Genero genero;
    private  Estado estado;
    
    public enum Estado{
        ESTUDIANTE, TRABAJADOR, JUBILADO, PARADO
    }

    public enum Genero{
        MASCULINO, FEMENINO, OTRO
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Personas adrian = new Personas("Adrián", 15, Genero.MASCULINO, Estado.ESTUDIANTE);
        
        Personas juan = new Personas("Juan");
        juan.saludar();
        juan.setEdad(20);
        System.out.println("La edad de Juan es: %d" juan.getEdad());
        adrian.saludar();
        
        adrian.cumplirAños();
        adrian.saludar();
        
        Personas p1 = new Personas();
        p1.saludar();
        Personas maria = new Personas("María", 55, Genero.FEMENINO, Estado.JUBILADO);
        System.out.println("Es María mayor que Juan?: " +maria.esMayor(juan));
        sumarUno(maria);
        
        maria.saludar();

        int valor = 1;

        sumarUno(valor);
        System.out.println("Valor: " + valor);

    }

    //Constructor
    public Personas(String nombreaux, int edadaux, Genero genero, Estado estado) {
        this.edad = edadaux;
        this.nombre = nombreaux;
        this.genero= genero;
        this.estado=estado;

    }
    //Sobrecarga el constructor

    public Personas(String nombre) {
        this.nombre = nombre;
        this.edad = 18;
        this.genero=Genero.OTRO;
       
    }

    public Personas() {
        this.nombre = "Desconocido";
        this.edad = 1;
        this.genero=Genero.OTRO;
        this.estado=Estado.PARADO;
    }

    
    
    
    //métodos
    String dameEstado(){
        String estadoEnCadena="";
        switch(this.estado){
            case ESTUDIANTE:
                estadoEnCadena="estudiante";
                break;
            case JUBILADO:
                estadoEnCadena="jubilado";
                break;
            case PARADO:
                estadoEnCadena="Buscando trabajo";
            case TRABAJADOR:
                estadoEnCadena="Trabajando";
                break;
        }
        return estadoEnCadena;
    }
    
    
    
    
    
    String dameGenero(){
        String generoEnCadena="";
        switch(this.genero){
            case MASCULINO:
                generoEnCadena="Hombre";
                break;
            case FEMENINO:
                generoEnCadena="Mujer";
                break;
            case OTRO:
                generoEnCadena="No binario";
                break;
               
                
                
             
                
        }
        return generoEnCadena;
    }
    public void saludar() {
        System.out.printf("Mi nombre es %s, tengo %d años. Soy %s. Estoy %s Encantado/a%n", this.nombre, edad, this.dameGenero(), this.dameEstado());
    }

    public void cumplirAños() {
        this.edad++;
    }
     public boolean esHombre(){
         return this.genero==Genero.MASCULINO;
     }
     
     public boolean esMayor(Personas p){
         return this.edad >p.edad;
     }

    static void sumarUno(int valor) {
        valor++;
        System.out.println("Valor dentro del método: " + valor);

    }

    static void sumarUno(Personas p) {
        p.cumplirAños();
    }

}
