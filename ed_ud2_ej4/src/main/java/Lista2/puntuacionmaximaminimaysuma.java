/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class puntuacionmaximaminimaysuma {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        
    Scanner s=new Scanner(System.in);
        int estudiantesSuspensos=0;
        int estudiantesAprobados=0;
        int estudiantesSignificativos=0;
        int estudiantesSobresalientes=0;
        double puntuacionMaxima = Double.MIN_VALUE;
        double puntuacionMinima = Double.MAX_VALUE;
        double sumaPuntuaciones = 0.0;
        int contadorEstudiantes = 0;
        
        while (true) {
        System.out.println("Introduce datos del estudiante");
        System.out.println("Nombre: (o * para terminar) ");
        String nombre=s.next();
         if (nombre.equalsIgnoreCase("*")) {
                break; 
            }
        
      System.out.println("Introduzca la puntuación: ");
        double puntuacion = s.nextDouble();
        s.nextLine();
        
        if(puntuacion<5){
            
            estudiantesSuspensos++;
       
            
        
        
        
    }else if(puntuacion>=5 && puntuacion<7){
       estudiantesAprobados++;
       
    }else if(puntuacion>=7 && puntuacion<8.5){
            estudiantesSignificativos++;
            
    }else {
        estudiantesSobresalientes++;
        
    }    
        if (puntuacion > puntuacionMaxima) {
                puntuacionMaxima = puntuacion;
            }

            if (puntuacion < puntuacionMinima) {
                puntuacionMinima = puntuacion;
            }

            sumaPuntuaciones += puntuacion;
            contadorEstudiantes++;
}
double puntuacionPromedio = sumaPuntuaciones / contadorEstudiantes;

        System.out.println("Número de estudiantes suspensos: " + estudiantesSuspensos);
        System.out.println("Número de estudiantes aprobados: " + estudiantesAprobados);
        System.out.println("Número de estudiantes significativos: " + estudiantesSignificativos);
        System.out.println("Número de estudiantes sobresalientes: " + estudiantesSobresalientes);
        System.out.println("Estudiante con la puntuacion mas alta: " + puntuacionMaxima);
        System.out.println("Estudiante con la puntuacion mas baja: " + puntuacionMinima);
        System.out.println("Puntuacion promedio de todos los estudiantes: " + puntuacionPromedio);
    }
}
