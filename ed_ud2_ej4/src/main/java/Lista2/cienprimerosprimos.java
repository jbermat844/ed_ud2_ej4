/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

/**
 *
 * @author Javi
 */
public class cienprimerosprimos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
int n = 2; // Iniciamos con el primer número primo, que es 2
        int suma = 0;
        int count = 0;

        while (count < 100) {
            if (esPrimo(n)) {
                suma += n;
                count++;
            }
            n++;
        }

        System.out.println("La suma de los primeros 100 números primos es: " + suma);
    }

    // Función para verificar si un número es primo
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        if (numero <= 3) {
            return true;
        }
        if (numero % 2 == 0 || numero % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i <= numero; i += 6) {
            if (numero % i == 0 || numero % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }
}
      
        
        
        
        
      
        
