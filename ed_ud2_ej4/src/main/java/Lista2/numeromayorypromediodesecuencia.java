/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class numeromayorypromediodesecuencia {

    public static void main(String[] args) {
        //Realiza un programa que lea una secuencia de números (terminará cuando se
//introduzca 0) y muestre al final:
//a) El número mayor introducido.
//b) El mensaje “Hay números negativos” si se han introducido números
//negativos.
//c) El promedio de los números positivos.
     


        Scanner s = new Scanner(System.in);
        
        int numero;
        int numeroMayor = 0;  // Inicializar con 0 en lugar de Integer.MIN_VALUE
        int sumaPositivos = 0;
        int cantidadPositivos = 0;
        boolean hayNegativos = false;
        
        System.out.println("Introduce una secuencia de números (termina con 0): ");
        
        do {
            numero = s.nextInt();
            
            if (numero != 0) {
                if (numero > numeroMayor) {
                    numeroMayor = numero;
                }
                
                if (numero < 0) {
                    hayNegativos = true;
                } else {
                    sumaPositivos += numero;
                    cantidadPositivos++;
                }
            }
        } while (numero != 0);
        
        if (numeroMayor != 0) {  
            System.out.println("El número mayor introducido es: " + numeroMayor);
        } else {
            System.out.println("No se ha introducido ningún número.");
        }
        
        if (hayNegativos) {
            System.out.println("Hay números negativos.");
        }
        
        if (cantidadPositivos > 0) {
            double promedioPositivos = (double) sumaPositivos / cantidadPositivos;
            System.out.println("El promedio de los números positivos es: " + promedioPositivos);
        }
        
        
    }
}
    

