/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class numerosamigos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //numeros amigos
        
    Scanner s = new Scanner(System.in);

        System.out.print("Ingrese el primer número positivo: ");
        int numero1 = s.nextInt();

        System.out.print("Ingrese el segundo número positivo: ");
        int numero2 = s.nextInt();

        int sumaDivisores1 = sumaDivisoresPropios(numero1);
        int sumaDivisores2 = sumaDivisoresPropios(numero2);

        if (sumaDivisores1 == numero2 && sumaDivisores2 == numero1) {
            System.out.println(numero1 + " y " + numero2 + " son números amigos.");
        } else {
            System.out.println(numero1 + " y " + numero2 + " no son números amigos.");
        }
    }

    public static int sumaDivisoresPropios(int numero) {
        int suma = 0;
        for (int i = 1; i <= numero / 2; i++) {
            if (numero % i == 0) {
                suma += i;
            }
        }
        return suma;
    }
}
