/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ganadorloteria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner=new Scanner(System.in);

        System.out.print("Introduce la cantidad a ser distribuida: ");
        double cantidadTotal = scanner.nextDouble();

        System.out.print("Introduce el número de ganadores en la categoría 3 aciertos: ");
        int ganadoresCategoria3 = scanner.nextInt();

        System.out.print("Introduce el número de ganadores en la categoría 4 aciertos: ");
        int ganadoresCategoria4 = scanner.nextInt();

        System.out.print("Introduce el número de ganadores en la categoría 5 aciertos: ");
        int ganadoresCategoria5 = scanner.nextInt();

        System.out.print("Introduce el número de ganadores en la categoría 6 aciertos: ");
        int ganadoresCategoria6 = scanner.nextInt();

        // Calcular la cantidad a atribuir a cada categoría
        double premioCategoria3 = cantidadTotal * 0.1 / ganadoresCategoria3;
        double premioCategoria4 = cantidadTotal * 0.2 / ganadoresCategoria4;
        double premioCategoria5 = cantidadTotal * 0.3 / ganadoresCategoria5;
        double premioCategoria6 = cantidadTotal * 0.4 / ganadoresCategoria6;

        // Mostrar los premios
        System.out.println("Premio por 3 aciertos: " + premioCategoria3);
        System.out.println("Premio por 4 aciertos: " + premioCategoria4);
        System.out.println("Premio por 5 aciertos: " + premioCategoria5);
        System.out.println("Premio por 6 aciertos: " + premioCategoria6);

       
    }
}
