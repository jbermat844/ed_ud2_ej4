/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Ejercicio11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
      


        
          Scanner scanner = new Scanner(System.in);
        int mujeresMenos700 = 0;
        int hombresMas800 = 0;
        boolean algunaMujerMas900 = false;

        while (true) {
            System.out.println("Ingrese los datos del empleado");
            System.out.print("Nombre (o 'salir' para terminar): ");
            String nombre = scanner.next();

            if (nombre.equalsIgnoreCase("salir")) {
                break; 
            }

            System.out.print("Género (M/F): ");
            String genero = scanner.next();

            System.out.print("Salario: ");
            double salario = scanner.nextDouble();

            System.out.print("Edad: ");
            int edad = scanner.nextInt();

            if (genero.equalsIgnoreCase("F") && salario > 500 && salario < 700) {
                mujeresMenos700++;
            }

            if (genero.equalsIgnoreCase("M") && salario > 800) {
                hombresMas800++;
            }

            if (genero.equalsIgnoreCase("F") && salario > 900) {
                algunaMujerMas900 = true;
            }
        }

        System.out.println("Mujeres que ganan menos de 700€: " + mujeresMenos700);
        System.out.println("Hombres que ganan más de 800€: " + hombresMas800);

        if (algunaMujerMas900) {
            System.out.println("Alguna mujer gana más de 900€.");
        } else {
            System.out.println("Ninguna mujer gana más de 900€.");
        }
    }
}
    
    


    
    

