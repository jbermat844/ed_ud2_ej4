/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

/**
 *
 * @author Javi
 */
public class erroressucesivosporsemana {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int semanas = 13;
        int erroresTotales = calcularErroresTotales(semanas);
        System.out.println("Número total de errores esperados al final del curso: " + erroresTotales);
    }

    public static int calcularErroresTotales(int semanas) {
        int errores = 0;
        int erroresSemanaAnterior = 1; 

        for (int semana = 1; semana <= semanas; semana++) {
            errores += 2 * erroresSemanaAnterior;
            erroresSemanaAnterior = 2 * erroresSemanaAnterior;
        }
        return errores;
    }
}
    
