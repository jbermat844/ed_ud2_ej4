/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class sexoMoF {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       // Realiza un programa que permita introducir la edad y el sexo de 60
//profesores. Debes validar que el sexo sea “M” o “F”. La edad debe estar
//comprendida entre 18 y 70 años. Se debe mostrar cuántos hombres y mujeres hay
//mayores de 45 años.
        
        
        
        Scanner s=new Scanner(System.in);
        int mujeresMayores=0;
        
        int numeroProfesores=60;
        int hombresMayores=0;
        
        for(int i=1; i<=numeroProfesores;i++){
            System.out.println("Introduce el sexo del profesor: " +i+ " (M o F)");
            String sexo=s.nextLine();
           if (!sexo.equals("M") && !sexo.equals("F")) {
                System.out.println("Sexo no válido. Debe ser 'M' o 'F'.");
                i--;
                continue;
            }
            System.out.println("Ingrese la edad del profesor: " +i+ " :");
            int edad=s.nextInt();
            
            if(edad<18||edad>70){
                System.out.println("Edad no válida. Debe estar entre 18 y 70");
                i--;
                continue;
            }
            if(sexo.equals("M")&edad>45){
                hombresMayores++;
                
            }else if(sexo.equals("F")&&edad>45){
                mujeresMayores++;
                
            }
        
        
        }
        System.out.println("Hombres mayores de 45: " +hombresMayores);
        System.out.println("Mujeres mayores de 45: " +mujeresMayores);
    }
    
}
