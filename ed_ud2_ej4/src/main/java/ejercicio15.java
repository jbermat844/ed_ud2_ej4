
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class ejercicio15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Desarrolla una función que devuelva verdadero o falso según si el carácter que
//recibe como parámetro es o no una vocal (vocal=verdadero, consonante=falso).
//Codifica una función principal donde se pidan de forma continuada caracteres
//(finaliza cuando se introduce 0) y compruebe para cada uno de ellos si se trata
//de una vocal o una consonante. Además tendrás que comprobar que lo que se
//introduce es una letra y no un dígito (a excepción del 0 que es para salir)
        
      Scanner s=new Scanner(System.in);
      
        char caracter=' ';
        do {
            System.out.print("Introduce un carácter (0 para salir): ");
            String cadena = s.next();

            if (cadena.length() == 1) {
                caracter = cadena.charAt(0);

                if (caracter == '0') {
                    System.out.println("Saliendo del programa. ¡Hasta luego!");
                } else {
                    if (Character.isLetter(caracter)) {
                        boolean esVocal = esVocal(caracter);
                        if (esVocal) {
                            System.out.println("'" + caracter + "' es una vocal.");
                        } else {
                            System.out.println("'" + caracter + "' es una consonante.");
                        }
                    } else {
                        System.out.println("Error: Debes introducir una letra.");
                    }
                }
            } else {
                System.out.println("Error: Introduce solo un carácter.");
            }

        } while (caracter != '0');

        
    }

    public static boolean esVocal(char letra) {
        letra = Character.toLowerCase(letra); // Convertir a minúscula para simplificar la comparación
        return letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u';
    }
}
        
        
        
        
    