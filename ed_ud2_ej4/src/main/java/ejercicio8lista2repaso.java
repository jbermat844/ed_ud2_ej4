/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class ejercicio8lista2repaso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa que calcule y muestre los números perfectos
//menores que 1000 (un número es perfecto si la suma de sus divisores,
//excepto el número en sí, es igual al número).
        
        System.out.println("Números perfectos menores que 1000: ");
        for(int i=1; i<=1000;i++){
            int sumaDivisores=0;
            
            for(int j=1;j<i;j++){
                if(i%j==0){
                    sumaDivisores+=j;
                    
                }
            }
            if(sumaDivisores==i){
                System.out.println(i);
            }
        }
        
        
    }
    
}
