/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class ejercicio4String {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce la cadena: ");
        String cadena = scanner.nextLine();

        int numVocales = contarVocales(cadena);

        System.out.println("Número de vocales en la cadena: " + numVocales);
    }

    public static int contarVocales(String cadena) {
        int numVocales = 0;
        cadena = cadena.toLowerCase(); // Convertir la cadena a minúsculas para considerar todas las vocales.

        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
                numVocales++;
            }
        }

        return numVocales;
    }
}