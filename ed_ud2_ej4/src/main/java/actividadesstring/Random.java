package actividadesstring;



import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class Random {

    /**
     * @param args the command line arguments
     */

  public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        generarNuevaPregunta(scanner);
        
    }

    public static void generarNuevaPregunta(Scanner scanner) {
        Random r = new Random();
        int num1 = r.nextInt(10) + 1; // Número aleatorio entre 1 y 10
        int num2 = r.nextInt(10) + 1; // Número aleatorio entre 1 y 10
        int respuesta;

        do {
            System.out.print("¿Cuánto es " + num1 + " por " + num2 + "? ");
            respuesta = scanner.nextInt();

            if (verificarRespuesta(num1, num2, respuesta)) {
                System.out.println("¡Muy bien!");
                num1 = r.nextInt(10) + 1;
                num2 = r.nextInt(10) + 1;
            } else {
                System.out.println("Inténtelo de nuevo.");
            }

        } while (true); // El bucle se repetirá hasta que la respuesta sea correcta
    }

    public static boolean verificarRespuesta(int num1, int num2, int respuesta) {
        return num1 * num2 == respuesta;
    }
}