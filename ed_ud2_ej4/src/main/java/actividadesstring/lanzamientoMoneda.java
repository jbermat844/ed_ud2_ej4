package actividadesstring;


import java.util.Random;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class lanzamientoMoneda {

    /**
     * @param args the command line arguments
     */
    

        

   
    

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int contadorCaras = 0;

        while (true) {
            System.out.println("1. Lanzar moneda");
            System.out.println("2. Salir");
            System.out.print("Seleccione una opción: ");
            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    boolean esCara = lanzarMoneda();
                    if (esCara) {
                        contadorCaras++;
                        System.out.println("¡Cara!");
                    } else {
                        System.out.println("Cruz");
                    }
                    break;
                case 2:
                    System.out.println("Número total de caras: " + contadorCaras);
                    System.out.println("Programa finalizado.");
                    System.exit(0);
                default:
                    System.out.println("Opción no válida. Inténtelo de nuevo.");
                    break;
            }
        }
    }

    public static boolean lanzarMoneda() {
        // Simula el lanzamiento de una moneda
        return Math.random() < 0.5;
    }
}