/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import static actividadesstring.contarPalabrasenCadena.contarPalabras;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class sumaDigitosentero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //- Realiza un método en Java para calcular la suma de los dígitos de un entero.
        
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce un número: ");
         int numero = s.nextInt();

        // Llama al método para calcular la suma de los dígitos
        int suma = calcularSumaDigitos(numero);

        // Muestra el resultado
        System.out.println("La suma es: " + suma);
    }

    public static int calcularSumaDigitos(int numero) {
        int suma = 0;

        // Calcula la suma de los dígitos
        while (numero != 0) {
            int digito = numero % 10;
            suma += digito;
            numero /= 10;
        }

        return suma;
    }
}
       
        
       
