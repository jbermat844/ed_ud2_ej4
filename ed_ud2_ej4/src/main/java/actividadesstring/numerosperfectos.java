/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

/**
 *
 * @author Usuario
 */
public class numerosperfectos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
  // System.out.println("Números perfectos menores que 1000:");
       // for (int i = 1; i < 1000; i++) {
           // int sumaDivisores = 0;
           // for (int j = 1; j < i; j++) {
              //  if (i % j == 0) {
                //    sumaDivisores += j;
                //}
           // }
           // if (sumaDivisores == i) {
             //   System.out.println(i);
            //}
       // }
   // }
//}
    for (int numero = 1; numero <= 1000; numero++) {
            if (idPerfecto(numero)) {
                System.out.print(numero + " es un número perfecto.");

                // Encuentra y muestra los factores del número perfecto
                for (int i = 2; i <= numero / 2; i++) {
                    if (numero % i == 0) {
                        System.out.print(" + " + i);
                    }
                }

                System.out.println();
            }
        }
    }

    public static boolean idPerfecto(int numero) {
        int sumaFactores = 1; // Incluye 1 como factor inicial

        for (int i = 2; i <= numero / 2; i++) {
            if (numero % i == 0) {
                sumaFactores += i;
            }
        }

        return sumaFactores == numero;
    }
}
