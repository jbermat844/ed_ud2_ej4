/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class contarPalabrasenCadena {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //- Realiza un método en Java para contar todas las palabras en una cadena.
//Datos de prueba:
//Introduce la cadena: el rápido zorro marrón salta sobre el perezoso perro.
//Salida esperada:
//Número de palabras en la cadena: 9
        
        
        
        
        
        Scanner s=new Scanner(System.in);
        System.out.print("Introduce la cadena: ");
        String cadena = s.nextLine();
        
        int numPalabras= contarPalabras(cadena);
        System.out.println("Número de palabras en la cadena: " + numPalabras);
        
        
    }
    public static int contarPalabras(String cadena) {
       
        String[] palabras = cadena.split(" "); // Dividir por espacios en blanco

        // Devuelve el número de palabras contadas
        return palabras.length;
    }
}