/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class areaCirculo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce el radio del círculo: ");
        double radio = scanner.nextDouble();

        double area = areaCirculo(radio);

        System.out.println("El área del círculo con radio " + radio + " es: " + area);
    }

    public static double areaCirculo(double radio) {
        // Fórmula para calcular el área de un círculo: A = π * r^2
        double area = Math.PI * radio * radio;
        return area;
    }
}
