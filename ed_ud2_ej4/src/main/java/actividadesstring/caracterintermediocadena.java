/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class caracterintermediocadena {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para mostrar el carácter intermedio de una
//cadena.
//Nota:
//a) Si la longitud de la cadena es par, habrá dos caracteres.
//b) Si la longitud de la cadena es impar, habrá un carácter intermedio.
//Datos de prueba:
//Introduce una cadena: 350
//Salida esperada:
//El carácter intermedio de la cadena es: 5
        
        
        
        
        
        
        
      Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce una cadena: ");
        String cadena = scanner.nextLine();

        mostrarCaracterIntermedio(cadena);
    }

    public static void mostrarCaracterIntermedio(String cadena) {
        int longitud = cadena.length();

        if (longitud % 2 == 0) {
            // La longitud es par, hay dos caracteres intermedios
            int indice1 = longitud / 2 - 1;
            int indice2 = longitud / 2;
            char caracter1 = cadena.charAt(indice1);
            char caracter2 = cadena.charAt(indice2);

            System.out.println("Los caracteres intermedios de la cadena son: " + caracter1 + " y " + caracter2);
        } else {
            // La longitud es impar, hay un único carácter intermedio
            int indiceIntermedio = longitud / 2;
            char caracterIntermedio = cadena.charAt(indiceIntermedio);

            System.out.println("El carácter intermedio de la cadena es: " + caracterIntermedio);
        }
    }
}