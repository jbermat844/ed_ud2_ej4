package actividadesstring;

import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
/**
 *
 * @author Usuario
 */
public class numeroInvertido {

    /**
     * @param args the command line arguments
     */
    public static int invertirNumero(int numero) {
        int numeroInvertido = 0;
        //El código utiliza un bucle while que se ejecutará siempre que numero 
        //sea diferente de cero. Esto sugiere que el propósito del bucle 
        //es procesar cada dígito del número original hasta que no queden dígitos.
        while (numero != 0) {
            //Se obtiene el último dígito del número original mediante 
            //el operador de módulo (%), que devuelve el resto de la división por 10.
            
            int digito = numero % 10;
          
            numeroInvertido = numeroInvertido * 10 + digito;
            numero /= 10;
        }

        return numeroInvertido;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Ingresa un número entero: ");

        int numero = s.nextInt();
        int numeroInvertido = invertirNumero(numero);

        System.out.println("Número con dígitos invertidos: " + numeroInvertido);
    }
}
