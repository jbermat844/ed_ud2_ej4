package actividadesstring;


import java.util.Random;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class Adivinanumero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    Scanner s = new Scanner(System.in);
        Random r = new Random();

        do {
            int numeroAdivinar = r.nextInt(1000) + 1;
            int intentos = 0;

            System.out.println("Adivina un número entre 1 y 1000.");

            while (true) {
                System.out.print("Introduce tu respuesta: ");
                int respuesta = s.nextInt();
                intentos++;

                if (respuesta == numeroAdivinar) {
                    System.out.println("¡Enhorabuena! ¡Adivinaste el número en " + intentos + " intentos!");
                    break;
                } else if (respuesta < numeroAdivinar) {
                    System.out.println("Demasiado bajo. Inténtelo de nuevo.");
                } else {
                    System.out.println("Demasiado alto. Inténtelo de nuevo.");
                }
            }

            System.out.print("¿Quieres jugar de nuevo? (s/n): ");
            char jugarDeNuevo = s.next().charAt(0);
            if (jugarDeNuevo != 's' && jugarDeNuevo != 'S') {
                break;
            }
        } while (true);

        System.out.println("¡Gracias por jugar!");
    }
}