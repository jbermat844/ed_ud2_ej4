package actividadesstring;


import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class promediode3numeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
     Scanner s = new Scanner(System.in);

        System.out.print("Introduce el primer número: ");
        double numero1 = s.nextDouble();

        System.out.print("Introduce el segundo número: ");
        double numero2 = s.nextDouble();

        System.out.print("Introduce el tercer número: ");
        double numero3 = s.nextDouble();

        double promedio = calcularPromedio(numero1, numero2, numero3);

        System.out.println("El valor promedio es " + promedio);
    }

    public static double calcularPromedio(double num1, double num2, double num3) {
        return (num1 + num2 + num3) / 3;
    }
}