package actividadesstring;


import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class menorde3numeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    Scanner s = new Scanner(System.in);

        System.out.print("Introduce el primer número: ");
        double numero1 = s.nextDouble();

        System.out.print("Introduce el segundo número: ");
        double numero2 = s.nextDouble();

        System.out.print("Introduce el tercer número: ");
        double numero3 = s.nextDouble();

        double menor = encontrarMenor(numero1, numero2, numero3);

        System.out.println("El valor más pequeño es " + menor);
    }

    public static double encontrarMenor(double num1, double num2, double num3) {
        double menor = num1;

        if (num2 < menor) {
            menor = num2;
        }

        if (num3 < menor) {
            menor = num3;
        }

        return menor;
    }
}