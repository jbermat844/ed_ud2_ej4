/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class numeromultiplodeotro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         //Realiza un método en Java esMultiplo que determine, para un par de
//enteros, si el segundo entero es múltiplo del primero. El método debe tomar dos enteros y devolver verdadero si el segundo es múltiplo del primero y
//falso en caso contrario. Introduce este método en un programa que introduzca
//una serie de pares de números enteros y determine si el segundo valor en cada
//par es múltiplo del primero.
        
        
        
        
        
   Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Introduce el primer número (o 0 para salir): ");
            int num1 = scanner.nextInt();

            if (num1 == 0) {
                break; // Salir si el usuario ingresa 0
            }

            System.out.print("Introduce el segundo número: ");
            int num2 = scanner.nextInt();

            if (esMultiplo(num1, num2)) {
                System.out.println(num2 + " es múltiplo de " + num1);
            } else {
                System.out.println(num2 + " no es múltiplo de " + num1);
            }
        }
    }

    public static boolean esMultiplo(int num1, int num2) {
        return num2 % num1 == 0;
    }
}