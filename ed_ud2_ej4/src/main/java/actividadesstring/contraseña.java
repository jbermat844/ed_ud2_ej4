/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class contraseña {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
          Scanner s = new Scanner(System.in);

        System.out.println("1. Una contraseña debe tener al menos ocho caracteres.");
        System.out.println("2. Una contraseña consta solo de letras y dígitos.");
        System.out.println("3. Una contraseña debe contener al menos 2 dígitos.");
        System.out.print("Introduce una contraseña (Usted acepta los términos y condiciones): ");
        String contrasena = s.nextLine();

        if (esContrasenaValida(contrasena)) {
            System.out.println("La contraseña es válida: " + contrasena);
        } else {
            System.out.println("La contraseña no es válida.");
        }
    }

    public static boolean esContrasenaValida(String contrasena) {
        // Comprueba si la contraseña tiene al menos ocho caracteres.
        if (contrasena.length() < 8) {
            return false;
        }

        int digitCount = 0;
        for (int i = 0; i < contrasena.length(); i++) {
            char c = contrasena.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                if (Character.isDigit(c)) {
                    digitCount++;
                }
            } else {
                return false; // Si se encuentra un carácter que no es letra ni dígito, la contraseña no es válida.
            }
        }

        return digitCount >= 2;
    }
}