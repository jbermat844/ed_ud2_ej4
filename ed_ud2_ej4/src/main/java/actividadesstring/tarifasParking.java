/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class tarifasParking {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
 Scanner scanner = new Scanner(System.in);
        double totalRecibos = 0;

        System.out.print("¿Cuántos clientes estacionaron ayer? ");
        int numClientes = scanner.nextInt();

        for (int i = 1; i <= numClientes; i++) {
            System.out.print("Introduce las horas de estacionamiento del cliente " + i + ": ");
            double horasEstacionamiento = scanner.nextDouble();

            double recibo = calculoCargos(horasEstacionamiento);
            totalRecibos += recibo;

            System.out.println("Recibo del cliente " + i + ": $" + recibo);
        }

        System.out.println("Total acumulado de recibos de ayer: $" + totalRecibos);
    }

    public static double calculoCargos(double horas) {
        double tarifaMinima = 2.00;
        double tarifaHora = 0.50;
        double tarifaMaxima = 10.00;

        if (horas <= 3) {
            return tarifaMinima;
        } else if (horas <= 24) {
            double horasExtras = horas - 3;
            double recibo = tarifaMinima + (horasExtras * tarifaHora);
            return Math.min(recibo, tarifaMaxima); // Límite máximo de $10.00
        } else {
            return tarifaMaxima; // Máximo de $10.00 por 24 horas
        }
    }
}