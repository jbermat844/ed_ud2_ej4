/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package actividadesstring;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class añoBisiesto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Scanner s=new Scanner(System.in);
        System.out.println("Introduce un año: ");
         int año = s.nextInt();
         

    boolean esBisiesto = esAñoBisiesto(año);

        if (esBisiesto) {
            System.out.println("verdadero");
        } else {
            System.out.println("falso");
        }
    }

    private static boolean esAñoBisiesto(int año) {
        
    if((año%4==0 && año%100!=0) || (año%400==0)){
            
             return true;
        } else {
            return false;
        }
    }
}
