/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Crea una función "Inicial", que devuelva la primera letra de una cadena de texto.
Prueba esta función para calcular la primera letra de la frase "Hola". */
        
         Scanner s = new Scanner(System.in);
         String cadena="Hola";
         
         char letraInicial=Inicial(cadena);
        
        System.out.println("La primera letra de la cadena " +cadena+ " es " +letraInicial);
        
        
    }

    private static char Inicial(String cadena) {
       
        char letraInicial = cadena.charAt(0);
        return letraInicial;
        
        
    }
    
}
