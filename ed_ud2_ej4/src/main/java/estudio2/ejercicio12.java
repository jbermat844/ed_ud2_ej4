/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Crea una función "UltimaLetra", que devuelva la última letra de una cadena de
texto. Prueba esta función para calcular la última letra de la frase "Hola". */

        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca una cadena: ");

        String cadena = s.nextLine();

        char letraFinal = ultimaLetra(cadena);

        System.out.println("La última letra de la cadena es: " + letraFinal);

    }

    private static char ultimaLetra(String cadena) {
        //Hay que verificar siempre que se introduzca la cadena por usuario que no está vacia
        if (cadena.isEmpty()) {
            return '\0';

        } else {
            return cadena.charAt(cadena.length() - 1);
        }

    }

}
