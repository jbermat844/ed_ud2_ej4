/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Implemente una función “subcadena” que dado un string, una posición de
comienzo y una cantidad de caracteres, devuelva una subcadena (substring) de
una cadena dada a partir de los parámetros dados (ej: entrada: “puntopeek”,
pos= 2, cant= 4; salida: “unto”.). Pide la cadena en el main y pásala como
referencia a la función. El resultado imprímelo en el main. */
        
        Scanner s = new Scanner(System.in);
        String cadena="puntopeek";
        
       String subcadena= subCadena(cadena);
       
        System.out.println(subcadena);

        
        
    }

    private static String subCadena(String cadena) {
        
       String subcadena=cadena.substring(1, 5);
        return subcadena;
    }
   
}
