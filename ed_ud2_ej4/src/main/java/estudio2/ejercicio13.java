/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Crea una función "PedirEntero", que reciba como parámetros el valor mínimo
aceptable y el valor máximo aceptable que se pide en el programa principal.
Deberá pedir al usuario que introduzca un valor dentro del rango anterior, en
caso de que el valor introducido no esté dentro del rango se lo volverá a pedir
hasta que sea correcto. Cuando esto suceda el mensaje de que el valor está
dentro del rango se imprimirá en la función principal.  */

        Scanner s = new Scanner(System.in);

        System.out.println("Introduzca el valor mínimo aceptable: ");
        int valorMínimo = s.nextInt();
        System.out.println("Introduzca el valor máximo aceptable: ");
        int valorMáximo = s.nextInt();
        pedirEntero(valorMínimo, valorMáximo, s);

    }

    private static void pedirEntero(int valorMínimo, int valorMáximo, Scanner s) {
        int valorDentroDelRango;
        do {

            System.out.println("Introduce un valor entre " + valorMínimo + " y" + valorMáximo + " :");
            valorDentroDelRango = s.nextInt();
            if (valorDentroDelRango > valorMáximo || valorDentroDelRango < valorMínimo) {
                System.out.println("El valor introducido no está en el rango. Inténtelo de nuevo");
            } else {
                System.out.println("El valor introducido está en el rango; por tanto, es correcto");
                return;
            }

        } while (true);

    }
}

/* while (true) se utiliza para crear un bucle que se ejecuta 
indefinidamente hasta que se alcanza una condición específica dentro del bucle que utiliza return para
salir de la función. En este caso, el bucle se ejecuta hasta que el valor introducido por el usuario está
dentro del rango especificado. Cuando eso sucede, el mensaje "Saliendo del programa." se imprime y la 
declaración return termina la ejecución de la función, saliendo así del bucle infinito y finalizando el
programa. */