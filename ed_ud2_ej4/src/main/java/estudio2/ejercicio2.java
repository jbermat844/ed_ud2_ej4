/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here4
        /*Realizar un programa que permita calcular el área de diferentes figuras
geométricas de forma que muestre el siguiente menú:
a. Círculo.
b. Cuadrado.
c. Rectángulo
d. Triángulo
e. Salir
Se deben usar funciones, una para cada área y las constantes como PI. Recuerda
que:
• Área del círculo = PI * radio2
• Área del cuadrado = lado2
• Área del rectángulo = ancho x alto
• Área del triángulo = base * altura / 2*/
        Scanner s = new Scanner(System.in);
        OUTER:
        while (true) {

            System.out.println("Menú de cálculo de áreas: ");
            System.out.println("a. Círculo");
            System.out.println("b. Cuadrado");
            System.out.println("c. Rectángulo");
            System.out.println("d.Triángulo");
            System.out.println("e. Salir");
            System.out.println("Elige una opción: (a/b/c/d/e :");
            String opcion = s.nextLine();

            switch (opcion) {
                case "a":
                    calcularAreaCírculo();
                    break;
                case "b":
                    calcularAreaCuadrado();
                    break;
                case "c":
                    calcularAreaRectángulo();
                    break;
                case "d":
                    calcularAreaTriángulo();
                case "e":
                    System.out.println("Saliendo del programa, Adiós");
                    break OUTER;
                default:
                    System.out.println("La opción no es válida, seleccione una opción correcta");
            }

        }

    }

    private static void calcularAreaCuadrado() {
       Scanner s = new Scanner(System.in);
        System.out.println("Introduce el lado del cuadrado: ");
        double lado=s.nextDouble();
        double area=Math.pow(2, lado);
        System.out.println("El área del círculo es : " +area);
    }

    private static void calcularAreaCírculo() {
        
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce el radio del círculo");
        int radio=s.nextInt();
        double area=Math.PI*(Math.pow(2, radio));
        System.out.println("El área del cuadrado es : " +area);
    }

    private static void calcularAreaRectángulo() {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce el ancho: ");
        int ancho=s.nextInt();
        System.out.println("Introduce el largo; ");
        int largo=s.nextInt();
        int area=ancho*largo;
        System.out.println("El área del rectángulo es : " +area);
    }

    private static void calcularAreaTriángulo() {
         Scanner s = new Scanner(System.in);
        System.out.println("Introduce la base: ");
        int base=s.nextInt();
        System.out.println("Introduce la altura; ");
        int altura=s.nextInt();
        int area=base*altura/2;
        System.out.println("El área del triángulo es : " +area);
    }

}
