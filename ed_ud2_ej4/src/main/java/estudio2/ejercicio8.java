/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*8. Crea una función "Cubo" que calcule el cubo de un número real (double) que se
indique como parámetro. El resultado deberá ser otro número real que tenga
solo 2 cifras de decimal y se debe mostrar en la función principal. Prueba esta
función para calcular el cubo de 3.2 y el de 5. */
        
        Scanner s = new Scanner(System.in);
        
        System.out.println("Introduzca un número real para que se calcule su cubo: ");
        double numero=s.nextDouble();
        
        double cuboDecimales=Cubo(numero);
        
        System.out.println("El resultado es: " +cuboDecimales);

        
        
        
    }

    private static double Cubo(double numero) {
        
        double cubo=Math.pow(numero, 3);
        double cuboDecimales=Math.round(cubo * 100.0)/100.0;
       return cuboDecimales;
    }
    
}
