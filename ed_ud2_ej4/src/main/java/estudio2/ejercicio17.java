/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Implemente una función llamada al revés que pida en el main un string
cualquiera, pase esa cadena a la función “revés” y muestre en el main un nuevo
string que sea el inverso. (ej: entrada: “puntopeek”; salida: “keepotnup”). */
        
        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduce un String: ");
        String cadena=s.nextLine();
        
        String cadenaInvertida= alReves(cadena);
        System.out.println(cadenaInvertida);
    }

    private static String alReves(String cadena) {
       
        int longitud = cadena.length();
        String cadenaInvertida="";
        for(int i=longitud-1;i>=0;i--){
            cadenaInvertida+=cadena.charAt(i);
            
        }
        return cadenaInvertida;
        
    }
    
}
