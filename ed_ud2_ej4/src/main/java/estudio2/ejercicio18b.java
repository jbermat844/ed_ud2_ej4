/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio18b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         /* Implemente una función “subcadena” que dado un string, una posición de
comienzo y una cantidad de caracteres, devuelva una subcadena (substring) de
una cadena dada a partir de los parámetros dados (ej: entrada: “puntopeek”,
pos= 2, cant= 4; salida: “unto”.). Pide la cadena en el main y pásala como
referencia a la función. El resultado imprímelo en el main. */
        
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce una cadena: ");
        String cadena=s.nextLine();
        System.out.println("Introduce la posición de inicio; ");
        int posicionInicio=s.nextInt();
        System.out.println("Introduce la cantidad de caracteres: ");
        int cantidadCaracteres=s.nextInt();
        
        String resultado=subCadena(cadena, posicionInicio, cantidadCaracteres);
        
        System.out.println(resultado);
        
        
    }

    private static String subCadena(String cadena, int posicionInicio, int cantidadCaracteres) {
        if(posicionInicio>=0 && cantidadCaracteres<=cadena.length() 
                && cantidadCaracteres>=0 && posicionInicio<=cadena.length()){
            return cadena.substring(posicionInicio, posicionInicio + cantidadCaracteres);
        }else{
            
        }
        return "Parámetros no válidos para devolver subcadena";
       
       
    }
   
}
