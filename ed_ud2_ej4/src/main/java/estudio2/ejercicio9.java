/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Crea una función "Menor" que calcule el menor de dos números enteros que
recibirá como parámetros. El resultado será otro número entero que será
devuelto a la función principal. Es similar a la función min de la clase Math.*/
        
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce el primer numero: ");
        int numero1=s.nextInt();
        System.out.println("Introduce el segundo numero: ");
        int numero2=s.nextInt();
        
        int numeroMenor=Menor(numero1, numero2);
        
        System.out.println("El numero menor es: " +numeroMenor);
        
        
        
        
    }

    private static int Menor(int numero1, int numero2) {
        
        int numeroMenor=Math.min(numero1, numero2);
        return numeroMenor;
        
    }
   
}
