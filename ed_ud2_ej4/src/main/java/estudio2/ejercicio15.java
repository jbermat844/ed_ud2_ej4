/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Desarrolla una función que devuelva verdadero o falso según si el carácter que
recibe como parámetro es o no una vocal (vocal=verdadero, consonante=falso).
Codifica una función principal donde se pidan de forma continuada caracteres
(finaliza cuando se introduce 0) y compruebe para cada uno de ellos si se trata
de una vocal o una consonante. Además tendrás que comprobar que lo que se
introduce es una letra y no un dígito (a excepción del 0 que es para salir)  */

        Scanner s = new Scanner(System.in);
        char letra;
        
            
              do {
            System.out.println("Introduce un caracter (0 para salir): ");
            letra = s.next().charAt(0);

            if (letra != '0') {
                if (esVocal(letra)) {
                    System.out.println("Verdadero");
                } else {
                    System.out.println("Falso");
                }
            }
        } while (letra != '0');
    }

    private static boolean esVocal(char letra) {

        if (Character.isDigit(letra)) {
             System.out.println("El valor introducido no es correcto");
            return false ;
        } else if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
            return true;
        } else {

            return false;
        }

    }

}
