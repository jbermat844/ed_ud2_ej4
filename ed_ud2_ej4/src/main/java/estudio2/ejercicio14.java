/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Crea una función "ContarLetra", que reciba una cadena y una letra, y devuelva
la cantidad de veces que dicha letra aparece en la cadena. Por ejemplo, si la
cadena es "Barcelona" y la letra es "a", debería devolver 2 (porque la "a" aparece
2 veces). */

        Scanner s = new Scanner(System.in);
        System.out.println("Introduce una cadena: ");
        String cadena = s.nextLine();
        System.out.println("Introduce la letra que quieres que se cuente: ");
        char letra = s.next().charAt(0);

        int cantidadLetra = contarLetra(cadena, letra);

        System.out.println("La letra " + letra + " aparece " + cantidadLetra + " veces ");

    }

    private static int contarLetra(String cadena, char letra) {
        int cantidadLetra = 0;
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == letra) {
                cantidadLetra++;
            }
        }
        return cantidadLetra;
    }
}
