/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para verificar si un año (entero) introducido por el
usuario es o no un año bisiesto.
Salida esperada:
Introduce el año: 2017
falso*/
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce un año y mostraré si es bisiesto");
        int año=s.nextInt();
        
        boolean añoBisiesto=esBisiesto(año);
        if(añoBisiesto){
            System.out.println("El año es bisiesto");
        }else{
            System.out.println("El año no es bisiesto");
        }
        
    }

    private static boolean esBisiesto(int año) {
        
        return ((año%4==0 && año%100!=0) ||año%400==0);
       
        
    }
    
}
