/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java que tome un valor entero y devuelva el número
con sus dígitos invertidos. Por ejemplo, dado el número 7631, el método debe
devolver 1367. Incorpora el método en un programa que lea un valor introducido
por el usuario y muestre el resultado.*/
        
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce un nùmero y lo invertiré: ");
        int numero=s.nextInt();
        
        int numeroInverso=numeroInvertido(numero);
        System.out.println("El número invertido es: " +numeroInverso);
        
        
    }

    private static int numeroInvertido(int numero) {
        int numeroInverso=0;
        while(numero!=0){
            int digito=numero%10;
             numeroInverso=numeroInverso*10+digito;
             numero/=10;
        }
        return numeroInverso;
    }
    
}
