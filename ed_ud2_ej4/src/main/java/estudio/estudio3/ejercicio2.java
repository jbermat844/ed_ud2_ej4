/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para calcular el promedio de 3 números.
Datos de prueba:
Introduce el primer número: 25
Introduce el segundo número: 45
Introduce el tercer número: 65
Salida esperada:
El valor promedio es 45.0*/
        
         Scanner scanner = new Scanner(System.in);
         int numero1=25;
         int numero2=45;
         int numero3=65;
         
         int promedio=promedioNumeros(numero1,numero2,numero3);
        System.out.println("El promedio es: " +promedio);
    }

    private static int promedioNumeros(int numero1, int numero2, int numero3) {
        int sumaNumeros=numero1+numero2+numero3;
        int promedio=sumaNumeros/3;
        return promedio;
        
    }
    
}
