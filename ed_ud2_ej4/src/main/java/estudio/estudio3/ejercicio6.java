/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para calcular la suma de los dígitos de un entero.
Datos de prueba:
        Introduce un número entero: 25
Salida esperada:
La suma es: 7

        */
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce un numero y sumaré los dígitos");
        int numero=s.nextInt();
        
        int suma=sumaDigitos(numero);
        System.out.println(suma);
        
        
    }

    private static int sumaDigitos(int numero) {
        int suma=0;
        while(numero>0){
            int digito=numero%10;
            suma+=digito;
            numero/=10;
                
            }
      
            return suma;
        }
        
        
    }
    

