/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para contar todas las palabras en una cadena.
Datos de prueba:
Introduce la cadena: el rápido zorro marrón salta sobre el perezoso perro.
Salida esperada:
Número de palabras en la cadena: 9*/
         Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce la cadena: ");
        String cadena = scanner.nextLine();
       int numeroPalabras=contarPalabras(cadena);
        System.out.println(numeroPalabras);
        
    }

    private static int contarPalabras(String cadena) {
        
        String[] palabras=cadena.split(" ");
        return palabras.length;
        
        
        }
    
}
