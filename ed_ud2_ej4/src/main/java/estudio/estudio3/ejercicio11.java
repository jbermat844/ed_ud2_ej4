/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java esMultiplo que determine, para un par de
enteros, si el segundo entero es múltiplo del primero. El método debe tomar dos
        argumentos enteros y devolver verdadero si el segundo es múltiplo del primero y
falso en caso contrario. Introduce este método en un programa que introduzca
una serie de pares de números enteros y determine si el segundo valor en cada
par es múltiplo del primero.*/
         Scanner s = new Scanner(System.in);
         System.out.println("Introduce el primer número: ");
         int numero=s.nextInt();
         System.out.println("Introduce el segundo número: ");
         int numero2=s.nextInt();
         
         if(esMultiplo(numero, numero2)){
             System.out.println("Son múltiplos");
    }else{
             System.out.println("No son múltiplos");
         }
         
         
    }

    private static boolean esMultiplo(int numero, int numero2) {
        if(numero%numero2==0){
            return true;
        }else{
            return false;
        }
    }
    
}
