/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para encontrar el número más pequeño entre 3
números.
Datos de prueba:
Introduce el primer número: 25
Introduce el segundo número: 37
Introduce el tercer número: 29
Salida esperada:
El valor más pequeño es 25.0*/
        
        
         Scanner scanner = new Scanner(System.in);
         int numero1=25;
         int numero2=37;
         int numero3=29;
         
          int resultado=valorMasPequeño(numero1, numero2, numero3);
                 System.out.println("El número más pequeño es: " +resultado);
    }

    private static int valorMasPequeño(int numero1, int numero2, int numero3) {
        
        int resultado=Math.min(Math.min(numero1, numero2),numero3);
        return resultado;
        
    }
    
}
