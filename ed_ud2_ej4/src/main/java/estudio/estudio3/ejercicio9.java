/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un método en Java para encontrar todos los números primos menores
que 100.
Salida esperada:
3,5,7,11,13,17,19,29,31,41,43,59,61,71,73*/

        Scanner s = new Scanner(System.in);

        for (int i = 2; i < 100; i++) {
            if (esPrimo(i)) {
                System.out.println(i);
            }
        }

    }

    private static boolean esPrimo(int numero) {
        for (int i = 2; i < numero; i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }

}
