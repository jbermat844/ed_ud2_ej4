/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio4b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para contar todas las vocales de una cadena.
//Datos de prueba:
//Introduce la cadena: w3resource
//Salida esperada:
//Número de vocales en la cadena: 4

        Scanner s = new Scanner(System.in);
        System.out.println("Introduce la cadena que desees: ");
        String cadena = s.nextLine();

        int numeroVocales = contarVocales(cadena);
        System.out.println("El numero de vocales de la cadena es: " + numeroVocales);

    }

    private static int contarVocales(String cadena) {
        int numVocales = 0;
        //Si hay Mayusculas es necesario
        //cadena = cadena.toLowerCase();
        for (int i = 1; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            if (caracter == 'a' || caracter == 'e' || caracter == 'e' || caracter == 'i' || caracter == 'u') {
                numVocales++;

            }

        }

        return numVocales;
    }

}
