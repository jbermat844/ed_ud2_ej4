/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para contar todas las palabras en una cadena.
//Datos de prueba:
//Introduce la cadena: el rápido zorro marrón salta sobre el perezoso perro.
//Salida esperada:
//Número de palabras en la cadena: 9
         Scanner s = new Scanner(System.in);
         
         String cadena=" el rápido zorro marrón salta sobre el perezoso perro";
         int numeroPalabras=contarPalabras(cadena);
         System.out.println("El número de palabras de la cadena es: " +numeroPalabras);
        
        
        
    }

    private static int contarPalabras(String cadena) {
        int numeroPalabras=0;
        for(int i=0;i<cadena.length();i++){
            char numeroEspacio=cadena.charAt(i);
            if(numeroEspacio==' '){
                numeroPalabras++;
                
            }
        }
        return numeroPalabras;
    }
    
}
