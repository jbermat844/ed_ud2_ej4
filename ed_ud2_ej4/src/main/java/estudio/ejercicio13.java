/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Se dice que un número entero es perfecto si sus factores, incluido 1 (excepto
//el número en sí), suman el número. Por ejemplo, 6 es un número perfecto, porque
//6 = 1 + 2 + 3. Realiza un método en Java llamado idPerfecto que determine si el
//número del parámetro es un número perfecto. Utiliza este método en un
//programa que calcule y muestre todos los números perfectos entre 1 y 1000.
//Muestra todos los factores de cada número perfecto para confirmar que el
//número es perfecto. 
        
         Scanner s=new Scanner(System.in);
      
         for(int numero=1;numero<=1000;numero++){
             if(idPerfecto( numero)){
                 System.out.println(numero+ " es un número perfecto");
                 
                 for(int i=1;i<=numero/2;i++){
                     if(numero%i==0){
                         System.out.print("+" +i);
                 }
                     
                     }
                     System.out.println();
         }
         }
         
      
    }

    private static boolean idPerfecto(int numero) {
       int sumaDivisores=1;
        for(int i=2;i<=numero/2;i++){
            if(numero%i==0){
                sumaDivisores+=i;
            }
            
        }
        return sumaDivisores==numero;
        
    }
    
}
