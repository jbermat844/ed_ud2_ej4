/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         //Realiza un método en Java para calcular la suma de los dígitos de un entero.
//Datos de prueba:
//Introduce un número entero: 25
//Salida esperada:
//La suma es: 7

    Scanner s = new Scanner(System.in);
         // Se solicita al usuario que introduzca un número en formato de cadena
        System.out.print("Introduce un número como cadena: ");
        String numeroComoCadena = s.nextLine();

        // Se llama a la función sumaDígitos y se almacena el resultado en la variable 'suma'
        int suma = sumaDígitos(numeroComoCadena);

        // Se imprime la suma de los dígitos
        System.out.println("La suma de los dígitos es: " + suma);
    }

    // Función para calcular la suma de los dígitos de una cadena
    private static int sumaDígitos(String numeroComoCadena) {
        int suma = 0;

        // Se itera a través de cada carácter de la cadena
        //Hay que empezar en 0 o solo coge el último numero
        for (int i = 0; i < numeroComoCadena.length(); i++) {
            char caracter = numeroComoCadena.charAt(i);

            // Se verifica si el carácter es un dígito
            // Character en mayuscula
            if (Character.isDigit(caracter)) {
                // Se convierte el carácter a entero y se suma al total
                suma += Character.getNumericValue(caracter);
            }
        }

        // Se devuelve la suma total de los dígitos
        return suma;
    }
}
