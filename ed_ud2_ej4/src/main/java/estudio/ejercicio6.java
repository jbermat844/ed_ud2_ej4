/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para calcular la suma de los dígitos de un entero.
//Datos de prueba:
//Introduce un número entero: 25
//Salida esperada:
//La suma es: 7

         Scanner s = new Scanner(System.in);
         int numero=25;
         int suma=sumaDígitos(numero);
         
        System.out.println("La suma de los dígitos es: " +suma);
        
    }

    private static int sumaDígitos(int numero) {
        int suma=0;
        
       // Se utiliza un bucle while para iterar a través de cada dígito del número
        while (numero != 0) {
            // Se obtiene el último dígito del número
            int ultimoDigito = numero % 10;

            // Se suma el dígito al total
            suma += ultimoDigito;

            // Se elimina el último dígito del número
            numero /= 10;
    }
        return suma;
    
}
}