/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       // Realiza un método en Java para encontrar el número más pequeño entre 3
//números.
//Datos de prueba:
//Introduce el primer número: 25
//Introduce el segundo número: 37
//Introduce el tercer número: 29
//Salida esperada:
//El valor más pequeño es 25.0

        Scanner s=new Scanner(System.in);
        int num1=25;
        int num2=37;
        int num3=39;
        
        int menor=masPequeño( num1, num2, num3);
     
        
        System.out.println("El número más pequeño es: " +menor );
        
    }

    private static int masPequeño(int num1, int num2, int num3) {
        int menor;
        if(num1<num2 && num1<num3){
            menor=num1;
        }else if(num2<num1 && num2<num3){
            menor=num2;
        }else{
            menor=num3;
        }
        return menor;
    }
    
    
}
