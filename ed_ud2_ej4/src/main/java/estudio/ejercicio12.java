/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java esMultiplo que determine, para un par de
//enteros, si el segundo entero es múltiplo del primero. El método debe tomar dos
//argumentos enteros y devolver verdadero si el segundo es múltiplo del primero y
//falso en caso contrario. Introduce este método en un programa que introduzca
//una serie de pares de números enteros y determine si el segundo valor en cada
//par es múltiplo del primero.
        
         Scanner s=new Scanner(System.in);
         
         System.out.println("Introduzca el primer número: ");
         int numero1=s.nextInt();
          System.out.println("Introduzca el segundo número: ");
         int numero2=s.nextInt();
         
         esMultiplo(numero1, numero2);
         if(esMultiplo(numero1, numero2)){
             System.out.println("Vedadero");
        //Modo más eficiente
        //boolean resultado=esMultiplo(numero1, numero2);
        //if(resultado){
        //System.out.println("Vedadero");
        
        
    }else{
             System.out.println("Falso");
         }
         
        
        
    }

    private static boolean esMultiplo(int numero1, int numero2) {
        return numero2%numero1==0;
    }
    
}
