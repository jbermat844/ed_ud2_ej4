/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que lea una secuencia de números (terminará cuando se
introduzca 0) y muestre al final:
a) El número mayor introducido.
b) El mensaje “Hay números negativos” si se han introducido números
negativos.
c) El promedio de los números positivos.*/

        Scanner s = new Scanner(System.in);

        int numero;
        int numeroMayor = 0;
        int promedioPositivos = 0;
        boolean hayNegativos = false;
        int sumaPositivos = 0;
        int totalPositivos = 0;
        System.out.println("Introduce una secuencia de números(terminará cuando se introduzca 0)");
        do {
            System.out.println("Introduce un número: ");
            numero = s.nextInt();
            if (numero != 0) {
                if (numero > numeroMayor) {
                    numeroMayor = numero;
                }
                if (numero < 0) {
                    hayNegativos = true;
                } else {

                    totalPositivos++;
                    sumaPositivos += numero;
                }

            }
            {

            }

        } while (numero != 0);
        promedioPositivos=sumaPositivos/totalPositivos;
        System.out.println("El promedio de los positivos es: " +promedioPositivos);
        System.out.println("El número mayor es: " +numeroMayor);
        System.out.println(hayNegativos);
    }

}
