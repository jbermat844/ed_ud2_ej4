/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /*Realiza un programa que permita introducir los nombres, género (M o
F), salario (>500 y <1000) y edad (>18 y <=65) de empleados de una
empresa. Deberá mostrar cuántas mujeres ganan menos de 700€ y cuántos
hombres ganan más de 800€. También deberá mostrar un mensaje diciendo
si alguna mujer gana más de 900€.*/
        Scanner s = new Scanner(System.in);
        int mujeresMenos700=0;
       
        int hombresMas800=0;
        

        while (true) {
            System.out.println("Introduce datos de los empleados");
            System.out.println("Introduce el nombre del empleado(* para salir)");
            String nombre = s.nextLine();
            if (nombre.equalsIgnoreCase("salir")) {
                break;
            }
             s.nextLine();
            System.out.println("Introduce la edad: ");
           
            int edad = s.nextInt();
            if(edad<=18 || edad>65){
                System.out.println("Edad incorrecta");
                break;
            }
            System.out.println("Introduce el género(M o F)");
            String genero = s.nextLine();
            s.nextLine();
            System.out.println("Introduce el salario: ");

            int salario = s.nextInt();
            if(salario<=500 || salario>=1000){
                break;
            }
           
            if(nombre.equalsIgnoreCase("F") && salario<700){
                mujeresMenos700++;
            }
            if(nombre.equalsIgnoreCase("M") && salario>800){
                hombresMas800++;
            }
        }
        System.out.println("El número de hombres que ganan mas de 800: " + hombresMas800);
         System.out.println("El número de mujeres que ganan menos de 700: " + mujeresMenos700);
    }

}
