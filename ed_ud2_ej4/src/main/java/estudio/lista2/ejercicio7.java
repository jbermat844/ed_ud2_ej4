/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir un número correspondiente
a un año y calcular y mostrar si es un año bisiesto o no. (Un año es bisiesto
si es un múltiplo de 4, excepto para los años que son múltiplos de 100 y no
son múltiplos de 400).
*/
        Scanner s=new Scanner(System.in);
        boolean esBisiesto=false;
        System.out.println("Introduce un año y te mostraré si es bisiesto");
        int año=s.nextInt();
        if((año%4==0 && año%100!=0) || año%400==0){
            esBisiesto=true;
        }else{
            esBisiesto=false;
        }
        
        System.out.println(esBisiesto);
    }
    
}
