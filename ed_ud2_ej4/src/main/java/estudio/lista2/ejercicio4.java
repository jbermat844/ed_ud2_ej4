/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir 2 números y mostrar su
producto calculado mediante sumas sucesivas.
         */

        Scanner s = new Scanner(System.in);
        int producto = 0;
        System.out.println("Introduzca el primer número: ");
        int numero1 = s.nextInt();
        System.out.println("Introduzca el segundo número: ");
        int numero2 = s.nextInt();
        for (int i = 1; i <= numero2; i++) {
            producto += numero1;
            System.out.print(numero1);

            if (i < numero2) {
                System.out.print("+");
            }
        }

        System.out.println("=" + producto);
    }

}
