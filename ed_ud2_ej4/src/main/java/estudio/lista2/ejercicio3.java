/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir la edad y el sexo de 60
profesores. Debes validar que el sexo sea “M” o “F”. La edad debe estar
comprendida entre 18 y 70 años. Se debe mostrar cuántos hombres y mujeres hay
mayores de 45 años.
*/
        Scanner s=new Scanner(System.in);
        String sexo="";
        int edad;
        int hombresMayoresDe45=0;
        int mujeresMayoresDe45=0;
         for (int i = 1; i <= 4; i++) {
            System.out.println("Introduce el sexo del estudiante (M o F)" + i + ":");
             sexo = s.nextLine();
            System.out.println("Introduce la edad del estudiante " + i + ":");
            edad = s.nextInt();
            s.nextLine();
            
            if(edad>=18 && edad<=70){
                if(edad>45 && "M".equals(sexo)){
                    hombresMayoresDe45++;
                
                }
                if(edad>45 && "F".equals(sexo)){
                  mujeresMayoresDe45++;
                
                }
                
            }else{
                System.out.println("El dato introducido no es correcto");
            }
                
            }
        System.out.println("El número de hombres mayores de 45 es: " +hombresMayoresDe45++);
        System.out.println("El número de mujeres mayores de 45 es: " +mujeresMayoresDe45++);
        
        
    }
    
}
