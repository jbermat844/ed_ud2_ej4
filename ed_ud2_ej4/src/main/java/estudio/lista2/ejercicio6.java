/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*Realiza un programa que permita introducir una puntuación numérica
(entre 0 y 10) y traducirlo al grado alfabético según la siguiente tabla:
0 <= GRADO < 3 MD
3 <= GRADO < 5 INS
5 <= GRADO < 6 SUF
6 <= GRADO< 7 BIEN
7 <= GRADO < 9 NOT
9 <= GRADO <= 10 SOB*/
         Scanner s=new Scanner(System.in);
         System.out.println("Introduzca una puntuación: ");
         int puntuacion=s.nextInt();
         String grado="";
         
         if(puntuacion>=0 && puntuacion<=3){
             grado="MD";
             
         }else if(puntuacion>=3 && puntuacion<5){
             grado="INS";
             
         }else if(puntuacion>=5 && puntuacion<6){
             grado="SUF";
         }else if(puntuacion>=6 && puntuacion<7){
             grado="BIEN";
                
                     }else if(puntuacion>=7 && puntuacion<9){
             grado="NOT";
                     }else if(puntuacion>=9 && puntuacion<=10){
             grado="SOB";
                     }else{
                         System.out.println("La puntuación no es correcta");
                     }
         System.out.println("El grado para" +puntuacion+ " es: " +grado);
    }
    
}
