/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que determine si 2 enteros positivos son amigos.
(Dos números son amigos si la suma de los divisores propios del primero es
igual al segundo y viceversa).
*/

         Scanner s=new Scanner(System.in);
         int sumaDivisores1=0;
         int sumaDivisores2=0;
         System.out.println("Introduzca el primer numero: ");
         int numero1=s.nextInt();
         System.out.println("Introduzca el segundo numero: ");
         int numero2=s.nextInt();
         
         for(int i=1;i<=numero1/2;i++){
             if(numero1%i==0){
                 sumaDivisores1+=i;
             }
         }
          for(int i=1;i<=numero2/2;i++){
             if(numero2%i==0){
                 sumaDivisores2+=i;
             }
         }
         if(sumaDivisores1==numero2 && sumaDivisores2==numero1){
             System.out.println("Los números son amigos");
         }else{
             System.out.println("Los numeros no son amigos");
         }

    }
    
}
