/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir el nombre y la edad de 50
estudiantes. Al final se debe mostrar la edad media, el nombre y la edad del
alumno de mayor edad.
         */

        Scanner s = new Scanner(System.in);
        int edadMedia = 0;
        int sumaEdades = 0;
        int edadAlumnoMayor = 0;
        int edad = 0;
        String nombreAlumnoMayor = "";

        for (int i = 1; i <= 4; i++) {
            System.out.println("Introduce el nombre del estudiante " + i + ":");
            String nombre = s.nextLine();
            System.out.println("Introduce la edad del estudiante " + i + ":");
            edad = s.nextInt();
            s.nextLine();

            if (edad != 0) {
                if (edad > edadAlumnoMayor) {
                    edadAlumnoMayor = edad;
                    nombreAlumnoMayor = nombre;

                }

            }
            sumaEdades += edad;
        }

        edadMedia = sumaEdades / 4;
        System.out.println("La edad media es: " + edadMedia);
        System.out.println("La edad del alumno mayor es: " + edadAlumnoMayor);
        System.out.println(nombreAlumnoMayor);
    }

}
