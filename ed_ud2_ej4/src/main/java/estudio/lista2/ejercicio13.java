/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que calcule y muestre la suma de los 100
primeros números primos.*/

        Scanner scanner = new Scanner(System.in);
        int contador = 0;
        int numero = 2;
        int suma = 0;
        while (contador < 100) {
            if (esPrimo(numero)) {
                System.out.println(numero);
                suma += numero;
                contador++;

            }
            numero++;
        }
        System.out.println("La suma es: " + suma);
    }

    private static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }

}
