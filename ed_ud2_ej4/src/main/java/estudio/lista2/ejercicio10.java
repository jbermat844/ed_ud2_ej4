/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista2;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir los nombres y las
puntuaciones en SPL (Lenguaje de Programación Estructurado) de los
estudiantes y muestre el número de:
a) Estudiantes suspensos (<5).
b) Estudiantes aprobados (>=5 y <7).
c) Estudiantes significativos (>=7 y <8.5).
d) Estudiantes sobresalientes (>=8.5 y <=10)
        Deberá mostrar el estudiante con la puntuación más alta, el estudiante con
la puntuación más baja y la puntuación promedio de todos los estudiantes.
La recopilación de datos finaliza cuando se introduce el carácter “*” en el
nombre de un estudiante.
         */

        Scanner s = new Scanner(System.in);
        
        int estudiantesSuspensos = 0;
        int estudiantesAprobados = 0;
        int estudiantesSignificativos = 0;
        int estudiantesSobresalientes = 0;
        int puntuacionAlta = Integer.MIN_VALUE;
        int puntuacionBaja = Integer.MAX_VALUE;
        int sumaPuntuaciones = 0;
        int contadorEstudiantes = 0;
        while (true) {
            System.out.println("Introduce datos del estudiante");
            System.out.println("Nombre: (o * para terminar) ");
            String nombre = s.next();
            if (nombre.equalsIgnoreCase("*")) {
                break;
            }
            System.out.println("Introduce la puntuación del alumno: ");
            int puntuacion = s.nextInt();
            if (puntuacion < 5) {
                estudiantesSuspensos++;
            } else if (puntuacion >= 5 && puntuacion < 7) {
                estudiantesAprobados++;
            } else if (puntuacion >= 7 && puntuacion < 8.5) {
                estudiantesSignificativos++;
            } else if (puntuacion >= 8.5 && puntuacion <= 10) {
                estudiantesSobresalientes++;
            } else {
                System.out.println("Los datos introducidos son incorrectos");
            }

            if (puntuacion > puntuacionAlta) {
                puntuacionAlta = puntuacion;
            }
            if (puntuacion < puntuacionBaja) {
                puntuacionBaja = puntuacion;
            }
            sumaPuntuaciones += puntuacion;
            contadorEstudiantes++;
        }
    

   
            int promedio = sumaPuntuaciones / contadorEstudiantes;

    System.out.println (

    "El número de estudiantes suspensos es: " + estudiantesSuspensos);
    System.out.println (

    "El número de estudiantes aprobados es: " + estudiantesAprobados);
    System.out.println (

    "El número de estudiantes significativos es: " + estudiantesSignificativos);
    System.out.println (

    "El número de estudiantes sobresalientes es: " + estudiantesSobresalientes);
    System.out.println (

    "El alumno con la puntuacion mas alta es: " + puntuacionAlta);
    System.out.println (

    "El alumno con la puntuacion más baja es: " + puntuacionBaja);
    System.out.println (

"El promedio es: " + promedio);

        }
        
    }
