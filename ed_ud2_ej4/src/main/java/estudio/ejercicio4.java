/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Realiza un método en Java para contar todas las vocales de una cadena.
//Datos de prueba:
//Introduce la cadena: w3resource
//Salida esperada:
//Número de vocales en la cadena: 4
        Scanner scanner = new Scanner(System.in);
        String cadena = "w3resource";

        int numVocales = contarVocales(cadena);
        System.out.println("El numero de vocales de la cadena es: " + numVocales);
    }

    private static int contarVocales(String cadena) {
        int numeroVocales = 0;
        //Si hay Mayusculas es necesario
        //cadena = cadena.toLowerCase();
        for (int i = 1; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
                numeroVocales++;
            }
        }
        return numeroVocales;

    }

}
