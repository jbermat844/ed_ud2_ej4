/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa en Java que solicite al usuario el radio de un círculo y
//utilice un método llamado areaCirculo para calcular el área del círculo.

        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduzca el radio de un círculo: ");
        double radio=s.nextInt();
        
        double area=areaCirculo(radio);
        System.out.println("El área del círculo con radio " +radio+ " es: " +area);
        
    }

    private static double areaCirculo(double radio) {
        double area=0;
        area=(int) (Math.PI*radio*radio);
        return area;
        
        
    }
    
    
    
    
}
