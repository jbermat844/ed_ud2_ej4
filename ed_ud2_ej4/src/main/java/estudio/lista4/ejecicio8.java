/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejecicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic hereç
        /*Crea una función "Cubo" que calcule el cubo de un número real (double) que se
indique como parámetro. El resultado deberá ser otro número real que tenga
solo 2 cifras de decimal y se debe mostrar en la función principal. Prueba esta
función para calcular el cubo de 3.2 y el de 5.*/
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce un número:");
        double numero = s.nextDouble();

        double numeroAlCuboRedondeado = Cubo(numero);
        System.out.println(numeroAlCuboRedondeado);

    }

    private static double Cubo(double numero) {

        double factorRedondeo = Math.pow(10, 2);
        double numeroAlCubo = Math.pow(numero, 3);
        double numeroAlCuboRedondeado = Math.round(numeroAlCubo * factorRedondeo) / factorRedondeo;
        return numeroAlCuboRedondeado;

    }

}
