/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Crea una función "DibujarRectanguloHueco" que dibuje en pantalla un
rectángulo hueco del ancho y alto que se indiquen como parámetros, formado 
        por una letra que también se indique como parámetro. Completa el programa
con un Main que pida esos datos al usuario y dibuje el rectángulo.

*/
         Scanner s=new Scanner(System.in);
         System.out.println("Introduce el alto");
         int alto=s.nextInt();
         System.out.println("Introduce el ancho");
         int ancho=s.nextInt();
         System.out.println("Introduce una letra ");
         char letra=s.next().charAt(0);
         
         dibujarCuadrado(alto, ancho, letra);
        
    }

    private static void dibujarCuadrado(int alto, int ancho, char letra) {
         for(int i=1;i<=alto;i++){
            for(int j=1;j<=ancho;j++){
                if(i==1 || i==alto || j==1 || j==ancho){
                    
                System.out.print(letra);
            }else{
                    System.out.print(" ");
                }
        }System.out.println();
    }
    }
    
}
