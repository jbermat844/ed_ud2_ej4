/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Crea una función "DibujarCuadrado" que dibuje en pantalla un cuadrado del
ancho (y alto) que se indique como parámetro. Para ello pide primero por
pantalla los datos y pásale el valor a la función. */
         Scanner s=new Scanner(System.in);
         System.out.println("Introduce el alto");
         int alto=s.nextInt();
         System.out.println("Introduce el ancho");
         int ancho=s.nextInt();
         
         dibujarCuadrado(alto, ancho);
        
    }

    private static void dibujarCuadrado(int alto, int ancho) {
        
        for(int i=1;i<=alto;i++){
            for(int j=1;j<=ancho;j++){
                System.out.print("*");
            }System.out.println();
        }
    }
    
}
