/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Escribir una función llamada redondear tal que acepte como parámetros un
número real y un número entero, siendo el primero de ellos el que debe
redondear, según se intuye en el nombre de la función. La función devolverá el
número redondeando la cantidad de posiciones decimales que establezca el
parámetro entero a través del propio parámetro real, es decir, cuando
realicemos la llamada: redondear (numero, posiciones); será en el número
donde se almacene el resultado.
Ejemplo: redondear(2.3658,2) → 2,37
*/
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce un numero real:");
        double numero=s.nextDouble();
        System.out.println("Introduce un numero entero: ");
        int posiciones=s.nextInt();
        
        double resultado=redondear(numero, posiciones);
        System.out.println("El número" + numero + " redondeado" +posiciones+ " posiciones es: " +resultado);
        
    }

    private static double redondear(double numero, int posiciones) {
        double factorRedondeo=Math.pow(10, posiciones);
        double resultado=Math.round(numero*factorRedondeo)/factorRedondeo;
        return  resultado;
    }
    
}
