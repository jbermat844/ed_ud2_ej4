/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Crea una función "PedirEntero", que reciba como parámetros el valor mínimo
aceptable y el valor máximo aceptable que se pide en el programa principal.
Deberá pedir al usuario que introduzca un valor dentro del rango anterior, en
caso de que el valor introducido no esté dentro del rango se lo volverá a pedir
hasta que sea correcto. Cuando esto suceda el mensaje de que el valor está
dentro del rango se imprimirá en la función principal.
         */
        Scanner s = new Scanner(System.in);

        System.out.println("Valor mínimo aceptable: ");
        int valorMínimo = s.nextInt();
        System.out.println("Valor máximo aceptable: ");
        int valorMáximo = s.nextInt();

        int valorUsuario = pedirEnter(valorMínimo, valorMáximo);

        if (valorUsuario > valorMínimo || valorUsuario < valorMáximo) {
            System.out.println("El valor" + valorUsuario + " está dentro del rango");
        }

    }

    private static int pedirEnter(int valorMínimo, int valorMáximo) {
        Scanner s = new Scanner(System.in);
        int valorUsuario;
        do {
            System.out.println("Introduzca un número dentro de los valores");
            valorUsuario = s.nextInt();

        } while (valorUsuario < valorMínimo || valorUsuario > valorMáximo);
        return valorUsuario;

    }

}
