/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista4;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Crea una función "UltimaLetra", que devuelva la última letra de una cadena de
texto. Prueba esta función para calcular la última letra de la frase "Hola".*/
        
         Scanner s = new Scanner(System.in);
         System.out.println("Introduce una cadena: ");
         String cadena=s.nextLine();
         
         ultimaLetra(cadena);
         System.out.println( ultimaLetra(cadena));
    }

    private static char ultimaLetra(String cadena) {
       
        if(cadena.length()>0){
           return cadena.charAt(cadena.length()-1);
            
        }else{
            System.out.println("Introduce una cadena válida");
            return 0;
        }
       
        
    }
    
}
