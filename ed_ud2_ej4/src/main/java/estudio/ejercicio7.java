/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para verificar si un año (entero) introducido por el
//usuario es o no un año bisiesto.
//Salida esperada:
//Introduce el año: 2017
//falso
        
        Scanner s = new Scanner(System.in);
        int año=2017;
        
        boolean añoBisiesto=esBisiesto(año);
        if(añoBisiesto){
            System.out.println("Verdadero");   
        }else{
            System.out.println("Falso");
        }
        
    }

    private static boolean esBisiesto(int año) {
        if((año%4==0 && año%100!=0) || (año%400==0)){
            return true;
        }else{
            return false;
        }
      
        
    }
    }