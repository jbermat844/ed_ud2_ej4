/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita introducir 3 números
diferentes y los muestre ordenados de menor a mayor.*/
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce tres números y los ordenaré de menor a mayor");
        System.out.println("Introduce el primer número: ");
        int numero1=s.nextInt();
        System.out.println("Introduce el segundo número: ");
        int numero2=s.nextInt();
        System.out.println("Introduce el tercer número: ");
        int numero3=s.nextInt();
            
          /*  if(numero1<numero2 && numero2<numero3){
                System.out.println("El orden de menor a mayor es: " +numero1+ "," +numero2+ "," +numero3 );
            }else if(numero1<numero2 && numero2>numero3 ){
                System.out.println("El orden de menor a mayor es: " +numero1+ "," +numero3+ "," +numero2 );
            }else if(numero2<numero1 && numero1<numero3){
                System.out.println("El orden de menor a mayor es: " +numero2+ "," +numero1+ "," +numero3 );
            }*/
        
        double min=Math.min(Math.min(numero1, numero2),numero3);
        double max=Math.max(Math.max(numero1, numero2),numero3);
        double medio=numero1+numero2+numero3 - min- max;
        System.out.println("El orden de menor a mayor es: " +min+ ", " +medio+ ", " +max);
}
}