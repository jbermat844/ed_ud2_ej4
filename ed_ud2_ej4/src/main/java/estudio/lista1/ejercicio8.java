/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que muestre la tabla de multiplicar
del número 1 al 10.*/

        Scanner s = new Scanner(System.in);
        int producto;
        int numero = 1;
        do {

            for (int i = 1; i <= 10; i++) {
                producto = numero * i;
                System.out.println(numero + "x" + i + "=" + producto);
            }
            numero++;
            System.out.println("Tabla del " + numero);
        } while (numero <= 10);
    }

}
