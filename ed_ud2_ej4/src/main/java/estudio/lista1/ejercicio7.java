/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que te permita introducir un
número y mostrar la tabla de multiplicar del mismo.*/
        
         Scanner s = new Scanner(System.in);
         int producto;
         int numero=0;
         System.out.println("Introduce un número y te mostraré su tabla de multiplicar: ");
          numero=s.nextInt();
         for(int i=1;i<=10;i++){
             producto=numero*i;
             System.out.println(numero+ "x" +i+ "=" +producto);
         }
        
    }
    
}
