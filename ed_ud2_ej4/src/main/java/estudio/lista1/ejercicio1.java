/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*  Realiza un programa que permita introducir dos números
por teclado y mostrarlos en orden de mayor a menor. Y mostrar
el texto “SON IGUALES”, en caso de igualdad. */
        
        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduzca el primer número: ");
        int numero1=s.nextInt();
        System.out.println("Introduzca el segundo número: ");
        int numero2=s.nextInt();
        
        if(numero1==numero2){
            System.out.println("SON IGUALES");
        }else if(numero1<numero2){
              System.out.println("El orden es: " +numero1+ "," +numero2);
        }else{
            System.out.println("El orden es: " +numero2+ "," +numero1);
        }

        
        
        
        
    }
    
}
