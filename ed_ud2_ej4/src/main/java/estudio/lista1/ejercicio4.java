/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que permita obtener la suma y el
producto de 10 números introducidos por teclado.*/
        Scanner s = new Scanner(System.in);
        int numero = 0;
        int suma = 0;
        int producto = 1;
        System.out.println("Introduzca 10 números  para obtener la suma y el producto:");
        for (int i = 1; i <= 10; i++) {
            System.out.println("Número " + i + ":");
            numero = s.nextInt();
            suma += numero;
            producto *= numero;
        }

        System.out.println("La suma es: " + suma);
        System.out.println("El producto es: " + producto);

    }

}
