/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que acepte 10 números por teclado
y muestre los siguientes resultados:
a.- La suma de los números mayores que 10.
b.- La suma de los números menores que 5.
c.- Cuántos números hay entre el 5 y el 10, ambos
incluidos.*/
        Scanner s = new Scanner(System.in);
        int numero;
        int mayoresQueDiez = 0;
        int menoresQueCinco = 0;
        int numerosEntre5y10 = 0;
        for (int i = 1; i <= 10; i++) {
            System.out.println("Introduza el numero " + i + ":");
            numero = s.nextInt();
            if (numero > 10) {
                mayoresQueDiez++;
            } else if (numero < 5) {
                menoresQueCinco++;
            } else if (numero >= 5 && numero <= 10) {
                numerosEntre5y10++;
            }
        }
        System.out.println("La cantidad de números mayores que 10 son: " + mayoresQueDiez);
        System.out.println("La cantidad de números menores que 10 son: " + menoresQueCinco);
        System.out.println("La cantidad de números entre 5 y 10 es:  " +numerosEntre5y10);
    }

}
