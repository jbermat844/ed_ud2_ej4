/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Realiza un programa que acepte una serie de números y
muestre cuántos números son mayores que 10. La serie
terminará cuando se introduzca el número 0.
         */
        Scanner s = new Scanner(System.in);
        int numero;
        int numerosMayores = 0;
        do {
            System.out.println("Introduzca una serie de números(finalizará cuando se introduzca 0):");
            numero = s.nextInt();
            if (numero > 10) {
                numerosMayores++;
            }

        } while (numero != 0);
        System.out.println("Los numeros mayores que diez son: " + numerosMayores);
    }

}
