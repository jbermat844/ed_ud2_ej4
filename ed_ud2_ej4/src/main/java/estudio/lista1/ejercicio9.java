/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio.lista1;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Realiza un programa que pida 20 números y muestre la
suma de los números que se piden en posición par y la
suma de los que se piden en posición impar. */
        
         Scanner s = new Scanner(System.in);
         int numero;
         int sumaPar=0;
         int sumaImpar=0;
         for(int i=1;i<=20;i++){
             System.out.println("Introduzca el número " +i+ ":");
             numero=s.nextInt();
             if(numero%2==0){
                 sumaPar+=numero;
             }else{
                 sumaImpar+=numero;
             }
         }
        System.out.println("La suma de los números en posición par es: " +sumaPar);
        System.out.println("La suma de los números en posición impar es: " +sumaImpar);
    }
    
}
