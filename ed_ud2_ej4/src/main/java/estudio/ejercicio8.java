/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para comprobar si una cadena es una contraseña
//válida.
//Reglas de contraseña:
//a) Una contraseña debe tener al menos ocho caracteres.
//b) Una contraseña consta solo de letras y dígitos.
//c) Una contraseña debe contener al menos 2 dígitos.
//Salida esperada:
//1. Una contraseña debe tener al menos ocho caracteres.
//2. Una contraseña consta solo de letras y dígitos.
//3. Una contraseña debe contener al menos 2 dígitos.
//Introduce una contraseña (Usted acepta los términos y condiciones): abcd1234
//La contraseña es válida: abcd1234
        
        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduce una contraseña (Usted acepta los térmimos y condiciones):");
        String contraseña=s.nextLine();
        
        esContraseña(contraseña);
        if( esContraseña(contraseña)){
            System.out.println("La contraseña es válida");
        }else{
            System.out.println("La contraseña no es válida");
        }
        
        
    }

    private static boolean esContraseña(String contraseña) {
        
        if(contraseña.length()<8){
            return false;
        }
        int cuentaDígito=0;
        for(int i=0;i<contraseña.length();i++){
            char caracter=contraseña.charAt(i);
            if(Character.isLetterOrDigit(caracter)){
                if(Character.isDigit(caracter)){
                    cuentaDígito++;
                }
            }else{
                return false;
            }
        }return cuentaDígito>=2;
        
    }
    
}
