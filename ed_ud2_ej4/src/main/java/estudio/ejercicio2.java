/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un método en Java para calcular el promedio de 3 números.
//Datos de prueba:
//Introduce el primer número: 25
//Introduce el segundo número: 45
//Introduce el tercer número: 65
//Salida esperada:
//El valor promedio es 45.0

        Scanner s = new Scanner(System.in);
        int num1 = 25;
        int num2 = 45;
        int num3 = 65;

        int promedio = valorPromedio(num1, num2, num3);

        System.out.println("El promedio de los tres números es: " + promedio);

    }

    private static int valorPromedio(int num1, int num2, int num3) {
        int sumaNumeros = 0;
        sumaNumeros = num1 + num2 + num3;
        int promedio = sumaNumeros / 3;
        return promedio;

    }

}
