
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class ejercicio6lista2repaso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa que permita introducir una puntuación numérica
//(entre 0 y 10) y traducirlo al grado alfabético según la siguiente tabla:
//0 <= GRADO < 3 MD
//3 <= GRADO < 5 INS
//5 <= GRADO < 6 SUF
//6 <= GRADO< 7 BIEN
//7 <= GRADO < 9 NOT
//9 <= GRADO <= 10 SOB 

        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduce una puntuación numérica y la traduciré al grado alfabético: ");
        int puntuación=s.nextInt();
        String gradoAlfabético="";
        
            if(puntuación>=0 && puntuación<=3){
                gradoAlfabético="MD";
                
            }else if(puntuación>=3 && puntuación<5){
                gradoAlfabético="INS";
            }else if(puntuación>=5 && puntuación<6){
                gradoAlfabético="SUF";
                
            }else if(puntuación>=6 && puntuación<7){
                gradoAlfabético="BIEN";
            }else if(puntuación>=7 && puntuación<9){
                gradoAlfabético="NOT";
            }else if(puntuación>=9 && puntuación<=10){
                gradoAlfabético="SOB";
            }else{
                System.out.println("Has introducido una puntuación incorrecta");
            }
        
        
        System.out.println("La puntuación " +puntuación+ " equivale a " +gradoAlfabético);
        
        
        
    }
    
}
