/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Poligonos;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class rombo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner s = new Scanner(System.in);
       int n = 7; // Número de filas
        int espacio = n / 2;

        // Mitad superior
        for (int i = 1; i <= n / 2; i++) {
            for (int j = 1; j <= espacio; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print("*");
            }
            espacio--;
            System.out.println();
        }

        espacio = 1; // Restablecer el valor de espacio

        // Mitad inferior
        for (int i = n / 2; i >= 1; i--) {
            for (int j = 1; j <= espacio; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print("*");
            }
            espacio++;
            System.out.println();
        }
    }
}
  
        
        
        
        
   