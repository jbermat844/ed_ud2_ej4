/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Poligonos;

/**
 *
 * @author Javi
 */
public class printfn {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
        // Impresión de cadenas con números
        String nombre = "Juan";
        int edad = 25;
        System.out.printf("Hola, %s. Tienes %d años.%n", nombre, edad);

        // Formateo de cadenas simples
        String producto = "Camiseta";
        double precio = 19.99;
        System.out.printf("%-15s: $%.2f%n", producto, precio);

        // Redondeo de números
        double numero = 123.456789;
        System.out.printf("Número redondeado: %.2f%n", numero);

        // Números en formato hexadecimal y octal
        int decimal = 255;
        System.out.printf("Decimal: %d, Hexadecimal: %X, Octal: %o%n", decimal, decimal, decimal);

        // Longitud y alineación de campos
        String nombre1 = "María";
        int edad1 = 30;
        double peso1 = 65.5;
        String nombre2 = "Pedro";
        int edad2 = 28;
        double peso2 = 75.2;
        String formato = "Nombre: %-10s, Edad: %d, Peso: %.2f%n";
        System.out.printf(formato, nombre1, edad1, peso1);
        System.out.printf(formato, nombre2, edad2, peso2);

        // Formato de fecha y hora
        Date fecha = new Date();
        System.out.printf("Hoy es %tF %tT%n", fecha, fecha);
    }
}
    }
    
}
