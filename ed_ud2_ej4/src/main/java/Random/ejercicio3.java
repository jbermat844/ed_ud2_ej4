/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Random;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Los ordenadores juegan un papel cada vez más importante en la educación.
Realiza un programa en Java que ayudará a un alumno de escuela primaria a
aprender a multiplicar. Usa un objeto aleatorio para generar 2 números enteros
positivos. A continuación, el programa debe realizar al usuario una pregunta,
como: ¿Cuánto es 6 por 7? Luego, el alumno, introduce la respuesta. Después, el
programa comprueba la respuesta del alumno. Si es correcta, muestra el mensaje
“Muy bien!” y hace otra pregunta de multiplicación. Si la respuesta es incorrecta,
muestra el mensaje “Inténtelo de nuevo” y permite que el alumno intente
responder la misma pregunta hasta que finalmente la conteste de forma correcta.
Se debe utilizar un método para generar cada nueva pregunta. Este método debe
llamarse una vez cuando el programa comience a ejecutarse y cada vez que el
usuario responda la pregunta correctamente.
*/
        Scanner s=new Scanner(System.in);
        Random r=new Random();
       while(true) {
        int numero1=r.nextInt(0, 10);
        int numero2=r.nextInt(0,10);
        
        System.out.println("Cuanto es: " +numero1+ " x " +numero2);
        int respuesta=s.nextInt();
        if(respuestaEsCorrecta(numero1, numero2, respuesta)){
            System.out.println("Muy bien, es correcto");
            
        }else{
            System.out.println("Intentelo de nuevo");
        }
        
    }
    }
    private static boolean respuestaEsCorrecta(int numero1, int numero2, int respuesta) {
        
        return respuesta==numero1*numero2;
        
    }
    
}
