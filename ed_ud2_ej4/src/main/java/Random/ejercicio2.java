/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Random;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*- Realiza un programa en Java que juegue a “adivina el número” de la
siguiente manera:
El programa elige el número a adivinar seleccionando un entero aleatorio en el
rango de 1 a 1000. El programa muestra el mensaje “Adivina un número entre 1
y 1000”.*/
        
        Scanner s=new Scanner(System.in);
        Random r=new Random();
        int numeroUsuario;
        int numero;
        
        System.out.println("Adivina un numero entre 1 y 1000");
        numero=r.nextInt(1, 1000);
        do{
        System.out.println("Intenta adivinar el número aleatorio introduciendo un numero: ");
        numeroUsuario=s.nextInt();
        if(numeroUsuario<numero){
            System.out.println("Demasiado bajo, intentelo de nuevo");
        }else if(numeroUsuario>numero){
            System.out.println("Demasiado alto, inténtelo de nuevo");
        }else{
            System.out.println("Es correcto");
            System.out.print("¿Quieres volver a jugar? (s/n): ");
            String jugarNuevamente = s.next().toLowerCase();

            if (!jugarNuevamente.equals("s")) {
                System.out.println("¡Gracias por jugar! Adiós.");
                break; // Salir del bucle principal si el usuario no quiere jugar de nuevo
            }
        }
        
        }while(numeroUsuario!=numero);  
    }
    
}
