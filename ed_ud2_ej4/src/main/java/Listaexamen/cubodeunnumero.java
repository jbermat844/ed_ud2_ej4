/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

/**
 *
 * @author Javi
 */
public class cubodeunnumero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       //Crea una función "Cubo" que calcule el cubo de un número real (double) que se
//indique como parámetro. El resultado deberá ser otro número real que tenga
//solo 2 cifras de decimal y se debe mostrar en la función principal. Prueba esta
//función para calcular el cubo de 3.2 y el de 5.

    double numero1 = 3.2;
        double numero2 = 5.0;

        double cubo1 = Cubo(numero1);
        double cubo2 = Cubo(numero2);

        System.out.println("El cubo de " + numero1 + " es " + cubo1);
        System.out.println("El cubo de " + numero2 + " es " + cubo2);
    }

    public static double Cubo(double numero) {
        double cubo = Math.pow(numero, 3);
        return Math.round(cubo * 100.0) / 100.0; // Redondear a dos decimales
    }
}