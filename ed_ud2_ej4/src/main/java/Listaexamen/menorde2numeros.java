/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class menorde2numeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función "Menor" que calcule el menor de dos números enteros que
//recibirá como parámetros. El resultado será otro número entero que será
//devuelto a la función principal. Es similar a la función min de la clase Math.
        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduzca el primer número: ");
        int num1=s.nextInt();
        
        System.out.println("Introduzca el segundo número: ");
        int num2=s.nextInt();
        
        int resultado=Menor(num1, num2);
        
        System.out.println("El número menor es: " +resultado);
        
        
    }

    private static int Menor(int num1, int num2) {
        if(num1<num2){
            return num1;
            
        }else{
            return num2;
        }
        
    }
    
}
