/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class pedirenterovalorminymax {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función "PedirEntero", que reciba como parámetros el valor mínimo
//aceptable y el valor máximo aceptable que se pide en el programa principal.
//Deberá pedir al usuario que introduzca un valor dentro del rango anterior, en
//caso de que el valor introducido no esté dentro del rango se lo volverá a pedir
//hasta que sea correcto. Cuando esto suceda el mensaje de que el valor está
//dentro del rango se imprimirá en la función principal.
        
        
         Scanner s=new Scanner(System.in);
         
         System.out.println("Introduce el valor mínimo que desees: ");
         int valorMínimo=s.nextInt();
         System.out.println("Introduce el valor máximo que desees: ");
         int valorMáximo=s.nextInt();
         
         int valorElegido=PedirEntero(valorMínimo, valorMáximo, s);
         
        System.out.println("El valor " +valorElegido+ " está dentro del rango.");
        
        
        
    }

    private static int PedirEntero(int valorMínimo, int valorMáximo, Scanner s) {
        int valor;
        do{
            System.out.println("Introduce un valor entre " +valorMínimo+ " y " +valorMáximo);
            valor=s.nextInt();
            
            if(valor<valorMínimo || valor>valorMáximo){
                System.out.println("El valor no está dentro del rango. Introduzca un valor correcto");
                
            }
            
                
            }while(valor<valorMínimo || valor>valorMáximo);
        return valor;
                
        }
    }
    

