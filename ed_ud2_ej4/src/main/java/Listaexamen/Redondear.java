/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

/**
 *
 * @author Javi
 */
public class Redondear {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    double numero = 2.3658;
        int posiciones = 2;
        double resultado = redondear(numero, posiciones);
        System.out.println(resultado);  // Debe mostrar 2.37
    }

    public static double redondear(double numero, int posiciones) {
        double factor = Math.pow(10, posiciones);  // Calcula el factor de redondeo
        double numeroRedondeado = Math.round(numero * factor) / factor;  // Redondea el número
        return numeroRedondeado;
    }
}