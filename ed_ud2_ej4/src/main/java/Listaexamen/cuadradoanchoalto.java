/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class cuadradoanchoalto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función "DibujarCuadrado" que dibuje en pantalla un cuadrado del
//ancho (y alto) que se indique como parámetro. Para ello pide primero por
//pantalla los datos y pásale el valor a la función. 
        
         Scanner s=new Scanner(System.in);
         
         System.out.println("Introduce el ancho del cuadrado: ");
         int ancho=s.nextInt();
         System.out.println("Introduce el largo del cuadrado: ");
         int largo=s.nextInt();
         DibujarCuadrado(ancho, largo);
        
    }

    private static void DibujarCuadrado(int ancho, int largo) {
        for(int i=0;i<largo;i++){
            for (int j=0; j<ancho;j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    
}
