/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

/**
 *
 * @author Javi
 */
public class cantidadletraenunacadena {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Crea una función "ContarLetra", que reciba una cadena y una letra, y devuelva
//la cantidad de veces que dicha letra aparece en la cadena. Por ejemplo, si la
//cadena es "Barcelona" y la letra es "a", debería devolver 2 (porque la "a" aparece
//2 veces).
        
        String cadena = "Barcelona";
        char letra = 'a';

        int cantidad = ContarLetra(cadena, letra);

        System.out.println("La letra '" + letra + "' aparece " + cantidad + " veces en la cadena.");
    }

    public static int ContarLetra(String cadena, char letra) {
        int contador = 0;

        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == letra) {
                contador++;
            }
        }

        return contador;
    }
}
        
        
        
   