/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Inicialesconysinsplit {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       // Crea una función "Iniciales", que reciba una cadena como "Marta Castaña" y
//devuelva las letras M y C (primera letra, y letra situada tras el primer espacio).
//Crea un "Main" que te pida la cadena y se pase a la función para que ésta
//imprima los resultados. Hazla dos versiones, una usando el método Split y otro
//sin usar ese método.

         Scanner scanner = new Scanner(System.in);

        // Pedir al usuario una cadena
        System.out.print("Ingresa una cadena: ");
        String cadena = scanner.nextLine();

        // Llamar a la función Iniciales con split
        InicialesConSplit(cadena);

       
    }

    // Función para obtener las iniciales con split
    static void InicialesConSplit(String cadena) {
        String[] palabras = cadena.split(" ");
        
        if (palabras.length >= 2) {
            char inicial1 = palabras[0].charAt(0);
            char inicial2 = palabras[1].charAt(0);

            System.out.println("Las iniciales son: " + inicial1 + " y " + inicial2);
        } else {
            System.out.println("No se encontraron suficientes palabras para obtener las iniciales.");
        }
    }
}
        
        // Función para obtener las iniciales sin split
    //static void InicialesSinSplit(String cadena) {
      //  int primerEspacio = cadena.indexOf(' ');

        //if (primerEspacio != -1 && primerEspacio < cadena.length() - 1) {
         //   char inicial1 = cadena.charAt(0);
         //   char inicial2 = cadena.charAt(primerEspacio + 1);

          //  System.out.println("Las iniciales son: " + inicial1 + " y " + inicial2);
    //    } else {
     //       System.out.println("No se encontró un espacio para obtener las iniciales.");
    //    }
   // }
//}
        
  