/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class cadenainvertida {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Implemente una función llamada al revés que pida en el main un string
//cualquiera, pase esa cadena a la función “revés” y muestre en el main un nuevo
//string que sea el inverso. (ej: entrada: “puntopeek”; salida: “keepotnup”).
        
    Scanner s = new Scanner(System.in);

        // Pedir al usuario una cadena
        System.out.print("Ingresa una cadena: ");
        String cadena = s.nextLine();

        // Llamar a la función alReves y mostrar el resultado
        String cadenaInvertida = alReves(cadena);
        System.out.println("Cadena invertida: " + cadenaInvertida);

    }

    // Función para invertir una cadena sin utilizar toCharArray ni StringBuilder
    static String alReves(String cadena) {
        int longitud = cadena.length();
        String cadenaInvertida = "";

        for (int i = longitud - 1; i >= 0; i--) {
            // Concatenar cada caracter al principio de la cadena invertida
            cadenaInvertida += cadena.charAt(i);
        }

        return cadenaInvertida;
    }
}