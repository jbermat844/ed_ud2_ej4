/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

/**
 *
 * @author Javi
 */
public class primeraletracadena {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función "Inicial", que devuelva la primera letra de una cadena de texto.
//Prueba esta función para calcular la primera letra de la frase "Hola".
        

        String texto = "Hola";
        
        char primeraLetra = Inicial(texto);
        
        System.out.println("La primera letra es: " + primeraLetra);
    }
    
    public static char Inicial(String cadena) {
        if (cadena.length() > 0) {
            return cadena.charAt(0);
        } else {
             
            System.out.println("La cadena está vacía");
            return 0;
        }
      
    
    }
}
  
        
        
   
