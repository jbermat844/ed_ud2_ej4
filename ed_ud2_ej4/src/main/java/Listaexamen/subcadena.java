/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class subcadena {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Implemente una función “subcadena” que dado un string, una posición de
//comienzo y una cantidad de caracteres, devuelva una subcadena (substring) de
//una cadena dada a partir de los parámetros dados (ej: entrada: “puntopeek”,
//pos= 2, cant= 4; salida: “unto”.). Pide la cadena en el main y pásala como
//referencia a la función. El resultado imprímelo en el main.

       Scanner scanner = new Scanner(System.in);

        // Pedir al usuario una cadena
        System.out.print("Ingresa una cadena: ");
        String cadena = scanner.nextLine();

        // Pedir posición de inicio y cantidad de caracteres
        System.out.print("Ingresa la posición de inicio: ");
        int posicionInicio = scanner.nextInt();
        System.out.print("Ingresa la cantidad de caracteres: ");
        int cantidadCaracteres = scanner.nextInt();

        // Llamar a la función subcadena y mostrar el resultado
        String resultado = subcadena(cadena, posicionInicio, cantidadCaracteres);
        System.out.println("La subcadena es: " + resultado);

        // Cerrar el scanner
        scanner.close();
    }

    // Función para obtener la subcadena
    static String subcadena(String cadena, int posicionInicio, int cantidadCaracteres) {
        // Verificar que la posición de inicio y la cantidad de caracteres sean válidos
        if (posicionInicio >= 0 && posicionInicio < cadena.length() &&
            cantidadCaracteres >= 0 && posicionInicio + cantidadCaracteres <= cadena.length()) {
            return cadena.substring(posicionInicio, posicionInicio + cantidadCaracteres);
        } else {
            return "Parámetros no válidos para obtener la subcadena.";
        }
    }
}