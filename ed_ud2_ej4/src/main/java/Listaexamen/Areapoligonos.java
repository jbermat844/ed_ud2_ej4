/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Areapoligonos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Realizar un programa que permita calcular el área de diferentes figuras
//geométricas de forma que muestre el siguiente menú:
//a. Círculo.
//b. Cuadrado.
//c. Rectángulo
//d. Triángulo
//e. Salir
//Se deben usar funciones, una para cada área y las constantes como PI. Recuerda
//que:
//• Área del círculo = PI * radio2
//• Área del cuadrado = lado2
//• Área del rectángulo = ancho x alto
//• Área del triángulo = base * altura / 2
        
        
        Scanner s=new Scanner(System.in);
        OUTER:
        while (true) {
            System.out.println("Menú de cálculo de áreas: ");
            System.out.println("a. Círculo");
            System.out.println("b. Cuadrado");
            System.out.println("c. Rectángulo");
            System.out.println("d.Triángulo");
            System.out.println("e. Salir");
            System.out.println("Elige una opción: (a/b/c/d/e");
            String opcion=s.nextLine();
            switch (opcion) {
                case "a":
                    calcularAreacirculo();
                    break;
                case "b":
                    calcularAreacuadrado();
                    break;
                case "c":
                    calcularArearectangulo();
                    break;
                case "d":
                    calcularAreatriangulo();
                    break;
                case "e":
                    //continuar=false; para evitar que el programa se repita
                    System.out.println("Saliendo del programa, Adiós");
                    break OUTER;
                default:
                    System.out.println("Opción no válida. Por favor, elige una opción válida (a/b/c/d/e).");
                    break;
            }
        }
        
        
        
        
        
        
        
    }

    private static void calcularAreacirculo() {
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce el radio del círculo: ");
        double radio=s.nextDouble();
        double area=Math.PI*radio;
        System.out.println("El area del círculo es: " +area);
        
    }

    private static void calcularAreacuadrado() {
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce el lado del cuadrado; ");
        double lado=s.nextDouble();
        double area=lado*lado;
        System.out.println("El área del cuadrado es: " +area);
        
        
        
    }

    private static void calcularArearectangulo() {
        Scanner s=new Scanner(System.in);
        System.out.println("Introduzca el ancho: ");
        double ancho=s.nextDouble();
        System.out.println("Introduce el largo: ");
        double largo=s.nextDouble();
        double area=ancho*largo;
        System.out.println("El area del rectangulo es: " +area);
        
        
    }

    private static void calcularAreatriangulo() {
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce la base: ");
        double base=s.nextDouble();
        System.out.println("Introduce la altura: ");
        double altura=s.nextDouble();
        double area=(base*altura)/2;
        System.out.println("El area del triángulo es: " +area);
        
        
    }
   
}
