/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class rectangulohueco {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función "DibujarRectanguloHueco" que dibuje en pantalla un
//rectángulo hueco del ancho y alto que se indiquen como parámetros, formado
//por una letra que también se indique como parámetro. Completa el programa
//con un Main que pida esos datos al usuario y dibuje el rectángulo.
        
        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce el ancho del rectángulo: ");
        int ancho = scanner.nextInt();
        System.out.print("Introduce el alto del rectángulo: ");
        int alto = scanner.nextInt();
        System.out.print("Introduce la letra para el rectángulo: ");
        char letra = scanner.next().charAt(0);

        DibujarRectanguloHueco(ancho, alto, letra);
    }

    public static void DibujarRectanguloHueco(int ancho, int alto, char letra) {
        for (int i = 1; i <= alto; i++) {
            for (int j = 1; j <= ancho; j++) {
                if (i == 1 || i == alto || j == 1 || j == ancho) {
                    System.out.print(letra);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}