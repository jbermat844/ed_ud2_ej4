/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ultimaletracadenaintroducidaporusuario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    //Crea una función "UltimaLetra", que devuelva la última letra de una cadena de
//texto introducida por usuario.

        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduce una cadena: ");
        String texto=s.nextLine();
        
        char ultimaletra=Ultimaletra(texto);
        
        System.out.println("La última letra de la cadena introducida es: " +ultimaletra);
    
    
    
    
    
    }

    private static char Ultimaletra(String cadena) {
        if(cadena.length()>0){
            return cadena.charAt(cadena.length()-1);
        }else{
            System.out.println("La cadena está vacía");
           return 0;
        }
    }

   
    
 }