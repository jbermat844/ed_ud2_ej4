/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class diaSiguiente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa que muestre por pantalla la fecha del día siguiente al que
//se especifica. Para ello:
//a. En el main deberás pedir el día y el mes al usuario y llamar a la función
//diaSiguiente()
//b. diaSiguiente(). Da solución al problema planteado llamando a otra
//función diasMes(mes), que es una función que especifica dependiendo
//el mes que sea, los días que tiene cada mes.
//c. Hazlo como estimes oportuno, aunque la fecha debe imprimirse en el
//main, y los valores deben guardarse en las variables día y mes que pediste
//al principio del ejercicio. IMPORTANTE: comprueba que el día siguiente
//del 31/12 es el 1/1!!
        
            Scanner s=new Scanner(System.in);
            
            System.out.println("Introduce un dia: ");
            int dia=s.nextInt();
            System.out.println("Introduce un mes: ");
            int mes=s.nextInt();
        
            diaSiguiente(dia,mes);
            
            
        
        
    }

    private static int diasMes(int mes) {
        int dia=0;
        
        switch(mes){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dia=31;
                break;
            case 2:
                dia=28;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dia=30;
               break;
            
        
    }
return dia;
   
        
    }

    private static void diaSiguiente(int dia, int mes) {
        int diaEnmesActual=diasMes(mes);
        
            if(dia<diaEnmesActual){
                dia++;
            }else{
                dia=1;
            }
            if(mes<12){
                mes++;
            }else{
                mes=1;
            }
        
        
        System.out.println("El dia siguiente es: " +dia+ " / " +mes);
    }
        
}
