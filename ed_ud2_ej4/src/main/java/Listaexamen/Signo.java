/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Signo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Crea una función llamada "Signo", que reciba un número real, y devuelva un
//número entero con el valor: -1 si el número es negativo, 1 si es positivo o 0 si es
//cero.

        Scanner s=new Scanner(System.in);
        
        System.out.println("Introduzca un número: ");
        int num=s.nextInt();
        
        int resultado=Signo(num);
        
        
        System.out.println("El número introducido corresponde al valor: " +resultado);
        
        
        
    }

    private static int Signo(int num) {
        if(num<0){
            return -1;
        }else if(num==0){
            return 0;
        }else{
            return 1;
        }
    }
    
}
