/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Listaexamen;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Consonantevocalodigito {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Desarrolla una función que devuelva verdadero o falso según si el carácter que
//recibe como parámetro es o no una vocal (vocal=verdadero, consonante=falso).
//Codifica una función principal donde se pidan de forma continuada caracteres
//(finaliza cuando se introduce 0) y compruebe para cada uno de ellos si se trata
//de una vocal o una consonante. Además tendrás que comprobar que lo que se
//introduce es una letra y no un dígito (a excepción del 0 que es para salir)
        
         Scanner s = new Scanner(System.in);
          char caracter;
       do {
            // Pedir al usuario un carácter
            System.out.print("Ingresa un carácter (0 para salir): ");

            // Leer un solo carácter
            String cadena = s.next();

            // Verificar si la entrada es válida
            if (cadena.length() == 1) {
                caracter = cadena.charAt(0);

                if (Character.isLetter(caracter)) {
                    // Llamar a la función esVocal y mostrar el resultado
                    if (esVocal(caracter)) {
                        System.out.println("'" + caracter + "' es una vocal.");
                    } else {
                        System.out.println("'" + caracter + "' es una consonante.");
                    }
                } else if (Character.isDigit(caracter)) {
                    System.out.println("'" + caracter + "' es un dígito.");
                } else {
                    System.out.println("Entrada no válida. Por favor, ingresa un carácter, letra o 0.");
                    caracter = '\0';
                }

            } else {
                // Mensaje de error si la entrada no es válida
                System.out.println("Por favor, ingresa un solo carácter válido.");
                caracter = '\0';
            }

        } while (caracter != '0');


        
    }

    // Función para verificar si un carácter es una vocal
    static boolean esVocal(char caracter) {
        // Convertir a minúscula para simplificar la comparación
        caracter = Character.toLowerCase(caracter);

        return caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u';
    }
}