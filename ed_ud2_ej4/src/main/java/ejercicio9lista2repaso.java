
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author Usuario
 */
public class ejercicio9lista2repaso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa que determine si 2 enteros positivos son amigos.
//(Dos números son amigos si la suma de los divisores propios del primero es
//igual al segundo y viceversa).

        Scanner s = new Scanner(System.in);
        
        System.out.println("Introduce el primer número: ");
        int num1=s.nextInt();
        System.out.println("Introduce el segundo número: ");
        int num2=s.nextInt();
        
        int sumaDivisores1=sumaDivisorespropios(num1);
        int sumaDivisores2=sumaDivisorespropios(num2);
        
        if(sumaDivisores1==num2 && sumaDivisores2==num1){
            System.out.println("El número " +num1+ "y el número " +num2+ " son amigos");   
            
        }else{
            System.out.println("El número " +num1+ "y el número " +num2+ " no son amigos");   
        }
        
        
        
        
    }

    private static int sumaDivisorespropios(int num) {
        int suma=0;
        for(int i=1; i<=num/2;i++){
            if(num%i==0){
                suma+=i;
                
            }
        }
        return suma;
    }
    
}
