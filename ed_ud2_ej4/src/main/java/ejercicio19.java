
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
/**
 *
 * @author Usuario
 */
public class ejercicio19 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Realiza un programa que muestre por pantalla la fecha del día siguiente al que
//se especifica. Para ello:
//a. En el main deberás pedir el día y el mes al usuario y llamar a la función
//diaSiguiente()
//b. diaSiguiente(). Da solución al problema planteado llamando a otra
//función diasMes(mes), que es una función que especifica dependiendo
//el mes que sea, los días que tiene cada mes.
//c. Hazlo como estimes oportuno, aunque la fecha debe imprimirse en el
//main, y los valores deben guardarse en las variables día y mes que pediste
//al principio del ejercicio. IMPORTANTE: comprueba que el día siguiente
//del 31/12 es el 1/1!!

        Scanner s = new Scanner(System.in);

        System.out.println("Introduce un día: ");
        int dia = s.nextInt();

        System.out.println("Introduce un mes: ");
        int mes = s.nextInt();

        DiaSiguiente(dia, mes);

         int diames = DiasMes(mes);

    }

    private static void DiaSiguiente(int dia, int mes) {
        int diaenMesActual=DiasMes(mes);
        
        
        // Verificamos si el día es válido
        if (dia < 1 || dia > diaenMesActual) {
            System.out.println("Error: Día no válido para el mes especificado.");
            return;
        }

        // Verificamos si es el último día del mes
        if (dia == diaenMesActual) {
            dia = 1;  // Si es el último día, el día siguiente es el primero del siguiente mes
            if (mes == 12) {
                mes = 1;  // Si es diciembre, el siguiente mes es enero del próximo año
            } else {
                mes++;  // De lo contrario, simplemente incrementamos el mes
            }
        } else {
            dia++;  // Si no es el último día del mes, simplemente incrementamos el día
        }

        // Mostramos la fecha del día siguiente
        System.out.println("La fecha del día siguiente es: " + dia + "/" + mes);
    }


    private static int DiasMes(int mes) {
        int dia = 0;
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dia = 31;
                break;
            case 2:
                dia = 28;
                break;
            default:
                dia = 30;
                break;
        }
        return dia;

    }

}
