/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Empleado {
  private   String nombre;
private String dirección;
private int añonacimiento;
private String dni;
private int añoantiguedad;

    public Empleado(String nombre, String dirección, int añonacimiento, String dni, double salarioanual, Departamento departamento, int añoantiguedad) {
        this.nombre = nombre;
        this.dirección = dirección;
        this.añonacimiento = añonacimiento;
        this.dni = dni;
        this.salarioanual = salarioanual;
        this.departamento = departamento;
        this.añoantiguedad = añoantiguedad;
    }

    public Empleado(String nombre, String dirección, int añonacimiento) {
        this.nombre = nombre;
        this.dirección = dirección;
        this.añonacimiento = añonacimiento;
    }
private double salarioanual;
private Departamento departamento;
public enum Departamento{
    INF, LOG, ADM, RRHH, FIN;
   

}

public boolean compruebaDepartamento(){
    return this.departamento==departamento;
}

public int calcularEdad(int edad, int añonacimiento){
    int añoActual=2023;
    return edad=añoActual-this.añonacimiento;
}
    public boolean esProgramador(Departamento departamento, double salarioanual){
        return this.departamento==Departamento.INF && salarioanual<=2000;
    }

    public int numeroTrabajadores(){
       switch (this.departamento) {
          case INF:
              return 4;
          case LOG:
              return 5;
          case ADM:
              return 10;
          case RRHH:
              return 2;
          case FIN:
              return 3;
          default:
              return 0;
      }
        
    }
public double salarioMensual(double salarioanual){
          return salarioanual/12;
      }
 public boolean posibilidadAscenso(int edad, int añoantiguedad) {
     return edad>40 && añoantiguedad>4;
        
    }
public boolean comprobarDni(String dni){
    return dni.length()==9 && dni.substring(0, 8).matches ("\\d+") && Character.isLetter(dni.charAt(8)) ;
    
}

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", direcci\u00f3n=" + dirección + ", a\u00f1onacimiento=" + añonacimiento + ", dni=" + dni + ", a\u00f1oantiguedad=" + añoantiguedad + ", salarioanual=" + salarioanual + ", departamento=" + departamento + '}';
    }


 public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el nombre del empleado: ");
        String nombre = scanner.nextLine();

        System.out.print("Ingrese la dirección del empleado: ");
        String direccion = scanner.nextLine();
        
        System.out.print("Ingrese el salario anual del empleado: ");
        double salarioAnual = scanner.nextDouble();
         System.out.print("Ingrese el año de nacimiento del empleado: ");
        int añonacimiento = scanner.nextInt();

        System.out.print("Ingrese el DNI del empleado: ");
        String dni = scanner.next();
        
        System.out.print("Ingrese el departamento del empleado: ");
        String departamento = scanner.next();

        System.out.print("Ingrese el año de antigüedad del empleado: ");
        int añoantiguedad = scanner.nextInt();

        Empleado p2=new Empleado(nombre, direccion, añonacimiento, dni, salarioAnual, Empleado.Departamento.ADM, añoantiguedad);
        
       
        p2.calcularEdad(añoantiguedad, añonacimiento);
        p2.comprobarDni(dni);
        p2.compruebaDepartamento();
        p2.esProgramador(Departamento.ADM, salarioAnual);
        p2.salarioMensual(salarioAnual);
        p2.numeroTrabajadores();
        p2.posibilidadAscenso(añoantiguedad, añoantiguedad);
        
        
}

}