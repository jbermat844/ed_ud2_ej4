/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Comprobaciones {
    private int numero;
    


public static boolean esPrimo(int numero){
      if(numero<=1){
          return false;
      }
      for(int i=2;i<=Math.sqrt(numero);i++){
          if(numero%i==0){
              return false;
          }
      }
        return true;
}

public static boolean esAntiguo(int numero){
    return numero<1900;
    
}

public static boolean esPar(int numero){
    return numero%2==0;
}

public static boolean estaIntervalo(int numero, int numeroMenor, int numeroMayor){
    return numero>=numeroMenor && numero<=numeroMayor;
}

public static boolean esPositivo(int numero){
    return numero>0;
}

public static boolean esBisiesto(int numero){
    return (numero%4==0 && numero%100!=0) || numero%400==0;
}

public static void main(String[] args){
    Scanner scanner=new Scanner(System.in);
    
     // Solicitar número para comprobar si es primo
        System.out.print("Ingrese un número para comprobar si es primo: ");
        int numPrimo = scanner.nextInt();
        System.out.println("¿Es primo? " + esPrimo(numPrimo));

        // Solicitar año para comprobar si es antiguo
        System.out.print("Ingrese un año para comprobar si es antiguo: ");
        int anioAntiguo = scanner.nextInt();
        System.out.println("¿Es antiguo? " + esAntiguo(anioAntiguo));

        // Solicitar número para comprobar si es par
        System.out.print("Ingrese un número para comprobar si es par: ");
        int numPar = scanner.nextInt();
        System.out.println("¿Es par? " + esPar(numPar));

        // Solicitar número y dos límites para comprobar si está en un intervalo
        System.out.print("Ingrese un número para comprobar si está en un intervalo: ");
        int numIntervalo = scanner.nextInt();
        System.out.print("Ingrese el límite inferior del intervalo: ");
        int limiteInferior = scanner.nextInt();
        System.out.print("Ingrese el límite superior del intervalo: ");
        int limiteSuperior = scanner.nextInt();
        System.out.println("¿Está en el intervalo? " + estaIntervalo(numIntervalo, limiteInferior, limiteSuperior));

        // Solicitar número para comprobar si es positivo
        System.out.print("Ingrese un número para comprobar si es positivo: ");
        int numPositivo = scanner.nextInt();
        System.out.println("¿Es positivo? " + esPositivo(numPositivo));

        // Solicitar año para comprobar si es bisiesto
        System.out.print("Ingrese un año para comprobar si es bisiesto: ");
        int añoBisiesto = scanner.nextInt();
        System.out.println("¿Es bisiesto? " + esBisiesto(añoBisiesto));
    }
}
