/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Persona {

    public Persona(String nombre, int edad, char sexo, double altura, double peso) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.altura = altura;
        this.peso = peso;
    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
    }
   private String nombre;
   private  int edad;
    private  char sexo;
    private double altura;
    private double peso;
    
    public static int calcularIMC(double altura, double peso){
       double imc=peso/Math.pow(altura, 2);
       if(imc<20){
           return -1;
       }else if(imc>=20 && imc<=25){
           return 0;
       }else{
           return 1;
       }
    }
    
    public boolean esMayorDeEdad(int edad){
        return edad>=18;
    }
    
    
    public boolean comprobarSexo(char sexo){
        return sexo=='M' || sexo=='F'; 
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + ", sexo=" + sexo + ", altura=" + altura + ", peso=" + peso + '}';
    }
    
    public static void main (String[] args){
        Scanner s=new Scanner(System.in);
        System.out.println("Introduce el nombre: ");
        String nombre=s.nextLine();
        System.out.println("Introduce la edad: ");
        int edad=s.nextInt();
        System.out.println("Introduce el sexo(M o F): ");
        char sexo=s.next().charAt(0);
        System.out.println("Introduce el peso en kg: ");
        double peso=s.nextDouble();
        System.out.println("Introduce la altura en m: ");
        double altura=s.nextDouble();
        
        Persona p1=new Persona(nombre, edad, sexo, altura, peso);
        
        int resultadoImc=Persona.calcularIMC(altura, peso);
        switch(resultadoImc){
            case 1:
                System.out.println("Tiene sobrepreso");
                break;
            case 0:
                System.out.println("Está en su peso ideal");
                break;
            case -1:
                System.out.println("Está por debajo de su peso ideal");
                break;
                
        }
        
        System.out.println(p1.comprobarSexo(sexo));
        System.out.println(p1.esMayorDeEdad(edad));
        System.out.println(p1.toString());
        
    } 
}
