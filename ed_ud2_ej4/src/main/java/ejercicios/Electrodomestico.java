/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

/**
 *
 * @author Javi
 */
public class Electrodomestico {
    
    private int precio;
    private Color color;
    private Consumo consumo;
    public enum Consumo{
        A, F
    }
    private double peso;
    public enum Color{
    BLANCO, ROJO, AZUL, GRIS
}

    public Electrodomestico(int precio, Color color, Consumo consumo, double peso) {
        this.precio = precio;
        this.color = color;
        this.consumo = consumo;
        this.peso = peso;
    }
    
    
    
}