/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Cine {
    String titulo;
    String director;
   
    int duracion;
    String resumen;
    String genero;
    
    public enum actorprincipal{
        George, Woody
    }
    
   
//Constructor de la clase
    public Cine(String titulo, String director, String actorprincipal, int duracion, String resumen, String genero) {
        this.titulo = titulo;
        this.director = director;
        
        this.duracion = duracion;
        this.resumen = resumen;
        this.genero = genero;
    }
        public void muestraDvdCine(){
            System.out.println("La película " +titulo+ " del aclamado director " +director+ " y protagonizada por " +actorprincipal.George+ " y " +actorprincipal.Woody+ "de duración " +duracion+ " y género " +genero );
        
        
    }
        public boolean esThriller(String genero){
          genero.toLowerCase();
        return genero.equalsIgnoreCase("Thriller");
          
            }
        
       public void tieneResumen(){
           if(resumen.isEmpty()){
               System.out.println("No tiene resumen");
           }else{
               System.out.println(resumen);
           }
        }
       
       public static void main(String[] args){
           Scanner s = new Scanner(System.in);
        System.out.print("Ingrese el título de la película: ");
        String titulo = s.nextLine();
        System.out.print("Ingrese el director de la película: ");
        String director = s.nextLine();
        
        System.out.print("Ingrese la duración de la película en minutos: ");
        int duracion = s.nextInt();
        s.nextLine();
        System.out.print("Ingrese el resumen de la película: ");
        String resumen = s.nextLine();
        System.out.print("Ingrese el género de la película: ");
        String genero = s.nextLine();
        
        Cine animalesNocturnos=new Cine(titulo, director, director, duracion, resumen, genero);
        animalesNocturnos.muestraDvdCine();
        
        //verificamos si es thriller
        if(animalesNocturnos.esThriller(genero)){
            System.out.println("Es thriller");
        }else{
            System.out.println("No es thriller");
        }
       //Verificamos si tiene resumen
       animalesNocturnos.tieneResumen();
       
       
       }
       
       
       
    
    
    
    
    
    
        
}
