/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class Alimento {
    
    private String nombre;
    private double contenidoEnLípidos;
     
    private double contenidoHidratos;
    private double contenidoProteinas;
    private boolean origenAnimal;
    private char contenidoVitaminas; // A alto, M medio, B bajo
    private char contenidoMinerales; // A alto, M medio, B bajo
public enum Contenido{
        A,M,B
    }
    public Alimento(String nombre) {
        this.nombre = nombre;
    }

    public Alimento(String nombre, double contenidoEnLípidos, double contenidoHidratos, double contenidoProteinas, boolean origenAnimal, char contenidoVitaminas, char contenidoMinerales) {
        this.nombre = nombre;
        this.contenidoEnLípidos = contenidoEnLípidos;
        this.contenidoHidratos = contenidoHidratos;
        this.contenidoProteinas = contenidoProteinas;
        this.origenAnimal = origenAnimal;
        this.contenidoVitaminas = contenidoVitaminas;
        this.contenidoMinerales = contenidoMinerales;
        
        
        
        
    }
    
    public boolean esDietetico(double contenidoEnLípidos,char contenidoVitaminas ){
        return contenidoEnLípidos<20 && contenidoVitaminas!='B';
            
        
    }

    @Override
    public String toString() {
        return "Alimento{" + "nombre=" + nombre + ", contenidoEnL\u00edpidos=" + contenidoEnLípidos + ", contenidoHidratos=" + contenidoHidratos + ", contenidoProteinas=" + contenidoProteinas + ", origenAnimal=" + origenAnimal + ", contenidoVitaminas=" + contenidoVitaminas + ", contenidoMinerales=" + contenidoMinerales + '}';
    }

   

    
   
   
    public double contenidoEnergético(){
        double caloriasLipidos=(contenidoEnLípidos*9.4)/100;
        double caloriasHidratos=(contenidoHidratos*4.1)/100;
        double caloriasProteinas=(contenidoProteinas*5.3)/100;
        return caloriasLipidos + caloriasHidratos + caloriasProteinas;
        
    }
    
    public boolean esRecomendableParaDeportistas(double contenidoProteinas, double contenidoHidratos, double contenidoEnLípidos ){
        return contenidoProteinas>=10 && contenidoProteinas<=15 && contenidoHidratos >=55 && contenidoHidratos<=65 && contenidoEnLípidos >=30 && contenidoEnLípidos <=35;
            
        }
    public static void main (String[] args){
         Scanner s = new Scanner(System.in);
         System.out.println("Introduce el nombre del alimento; ");
         String nombre=s.nextLine();
         Alimento alimento1=new Alimento(nombre);
         
         System.out.print("Ingrese el contenido en lípidos (%): ");
        double contenidoLipidos = s.nextDouble();
        System.out.print("Ingrese el contenido en hidratos de carbono (%): ");
        double contenidoHidratos = s.nextDouble();
        System.out.print("Ingrese el contenido en proteínas (%): ");
        double contenidoProteinas = s.nextDouble();
        System.out.print("¿Es de origen animal? (true/false): ");
        boolean origenAnimal = s.nextBoolean();
        System.out.print("Ingrese el contenido en vitaminas (A/M/B): ");
        char contenidoVitaminas = s.next().charAt(0);
        System.out.print("Ingrese el contenido en minerales (A/M/B): ");
        char contenidoMinerales = s.next().charAt(0);
        
        alimento1=new Alimento(nombre, contenidoLipidos, contenidoHidratos, contenidoProteinas, origenAnimal, contenidoVitaminas, contenidoMinerales);
        
        System.out.println(alimento1.toString());
        alimento1.esDietetico(contenidoLipidos, contenidoVitaminas);
        alimento1.esRecomendableParaDeportistas(contenidoProteinas, contenidoHidratos, contenidoLipidos);
        alimento1.contenidoEnergético();
        
        
    }
}
