/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejercicios.period;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

/**
 *
 * @author Javi
 */
public class ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Fecha y hora de nacimiento
        LocalDate fechadenacimiento=LocalDate.of(1981, 9,1);
        LocalTime horaDeNacimiento=LocalTime.of(8, 0);
        //Hora actual
        LocalDateTime fechaHoraActual=LocalDateTime.now();
        //Periodo desde nacimiento a fecha actual
        Period p=Period.between(fechadenacimiento, fechaHoraActual.toLocalDate());
        
       //Duracion desde la hora de nacimiento hasta la actual
       Duration d=Duration.between(horaDeNacimiento, fechaHoraActual.toLocalTime());
       if(d.isNegative()){
           p.minusDays(1);
           d.plusDays(1);
       }
        
        long horas=d.toHours();
        long minutos=d.toMinutes()%60;
        long segundos=d.getSeconds()%60;
        
        
        
        
    }
    
}
