/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Realiza un programa que pida la cantidad de números que quiere introducir el usuario, y 
luego los vaya pidiendo uno a uno para finalmente mostrar el número menor de los introducidos por 
el usuario. */

        Scanner s = new Scanner(System.in);

        System.out.println("Indica la cantidad de números que quieres introducir: ");
        int cantidadNúmeros = s.nextInt();
        //Siempre comprobar que existe al menos un número
        if (cantidadNúmeros > 0) {
            int menor = Integer.MAX_VALUE;

            for (int i = 1; i <= cantidadNúmeros; i++) {
                System.out.print("Introduce el número " + i + " :");
                int numero = s.nextInt();
                if (numero < menor) {
                    menor=numero;
                }

            }
            System.out.println("El menor introducido es: " +menor);
        }else{
            System.out.println("Debes introducir al menos un número");
        }

    }

}
