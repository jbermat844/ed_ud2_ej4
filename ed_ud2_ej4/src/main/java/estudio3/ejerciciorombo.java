/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejerciciorombo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Realiza un programa utilizando bucles que muestre las siguientes figuras: */
        Scanner s = new Scanner(System.in);
        int numero = 7;
        int espacio = numero / 2;
        for (int i = 1; i <= numero / 2; i++) {

            for (int j = 1; j <= espacio; j++) {
                System.out.print(" ");
            }
            for(int j=1;j<=2*i-1;j++){
                System.out.print("*");
            }
            espacio--;
            System.out.println();
        }
        espacio=1;
        
        for (int i = numero/2; i >=1 ; i--) {

            for (int j = 1; j <= espacio; j++) {
                System.out.print(" ");
            }
            for(int j=1;j<=2*i-1;j++){
                System.out.print("*");
            }
            espacio++;
            System.out.println();
    }

}
}