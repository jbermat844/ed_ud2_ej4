/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class tablasmultiplicar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //  Realiza un programa que muestre por pantalla las tablas de multiplicar del 1 al 10 

        Scanner s = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {
            System.out.println("Tabla del " + i);
            System.out.println("**********");

            for (int j = 1; j <= 10; j++) {
                int resultado = i * j;
                System.out.println(i + " x " + j + " = " + resultado);
            }

            System.out.println(); // Salto de línea entre las tablas
        }
    }
}


    
}
