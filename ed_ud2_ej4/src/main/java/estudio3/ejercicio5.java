/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package estudio3;

import java.util.Scanner;

/**
 *
 * @author Javi
 */
public class ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*  Realiza un programa que pida números hasta que uno sea cero.
En cada interacción (desde la segunda) debe imprimir la suma de los dos últimos números. */

        Scanner s = new Scanner(System.in);
        int numeroActual;
        int numeroAnterior=0;
        int contador=0;
        
        do{
            System.out.println("Introduce un número: ");
            numeroActual=s.nextInt();
            
            if(contador>=1 && numeroAnterior!=0){
                int suma=numeroActual+numeroAnterior;
                System.out.println("La suma es: " +suma);
            }
            contador++;
            numeroAnterior=numeroActual;
                
        }while(numeroActual!=0);
        
        System.out.println("Programa finalizado");
        
    }
}