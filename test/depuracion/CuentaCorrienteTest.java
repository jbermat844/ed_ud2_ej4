/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package depuracion;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnavarro
 */
public class CuentaCorrienteTest {
    private static CuentaCorriente cuentaDestino;
    CuentaCorriente cuentaPrueba;
    
    public CuentaCorrienteTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        cuentaDestino=new CuentaCorriente("destino desconocido", 100);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        cuentaPrueba=new CuentaCorriente("ejemplo", 1000);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test
    public void testIngresa() {
        System.out.println("ingresa");
        double i = 200.0;
        CuentaCorriente instance = new CuentaCorriente(100);
        instance.ingresa(i);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertEquals(300, instance.getSaldo(), i);
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae() {
        System.out.println("extrae");
        double s = cuentaPrueba.getSaldo();
        double e=150;
        //CuentaCorriente instance = new CuentaCorriente();
        cuentaPrueba.extrae(e);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertEquals(s-e, cuentaPrueba.getSaldo());
    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferir() {
        System.out.println("tranferir");
        //CuentaCorriente destino = null;
        double cantidad = 120.0;
        double d=cuentaDestino.getSaldo();
        double cantidadInicial=200;
        CuentaCorriente instance = new CuentaCorriente(cantidadInicial);
        instance.tranferir(cuentaDestino, cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        //assertEquals(cantidadInicial-cantidad,instance.getSaldo());
        assertTrue((d+cantidad== cuentaDestino.getSaldo())&&(cantidadInicial-cantidad==instance.getSaldo()));
    }
    
}
