/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package depuracion;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnavarro
 */
public class EcuacionSegundoGradoTest {
    
    public EcuacionSegundoGradoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of raices method, of class EcuacionSegundoGrado.
     */
    /**
     * Test of raices method, of class EcuacionSegundoGrado.
     */
    @Test
    public void testRaices() {
        System.out.println("raices");
        int a = 1;
        int b = 0;
        int c = -1;
        String expResult = "La ecuación tiene dos soluciones:" + "-1.0" +" y "+ "1.0";
        String result = EcuacionSegundoGrado.raices(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }    
    @Test
    public void testRaices2() {
        System.out.println("raices");
        int a = 1;
        int b = 0;
        int c = 0;
        String expResult = "La ecuación tiene una única solución doble que es: 0";
        String result = EcuacionSegundoGrado.raices(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }    
    /**
     * Test of raices method, of class EcuacionSegundoGrado.
     */
    @Test
    public void testRaices3() {
        System.out.println("raices");
        int a = 1;
        int b = 0;
        int c = 1;
        String expResult = "La ecuación no tiene solución";
        String result = EcuacionSegundoGrado.raices(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }       
    public void testRaices4() {
        System.out.println("raices");
        int a = 1;
        int b = 0;
        int c = 1;
        String expResult = "La ecuación no tiene solución";
        String result = EcuacionSegundoGrado.raices(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }       

    /**
     * Test of main method, of class EcuacionSegundoGrado.
     */
    @Test
    public void testMain() {
        /*
        System.out.println("main");
        String[] args = null;
        EcuacionSegundoGrado.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
*/
    }
}
